pico-8 cartridge // http://www.pico-8.com
version 8
__lua__

----------------------
-- save yourself v.1.0c by dagondev (http://www.lexaloffle.com/bbs/?tid=28309) --
----------------------
----------------------
-- acknowledgements --
-- data composition based on, collision system, snow and clouds rendering taken, from game: celeste by matt thorson + noel berry: http://www.lexaloffle.com/bbs/?tid=2145 --
-- string data storage, and functions for those, based on this post: http://www.lexaloffle.com/bbs/?tid=28160
----------------------

----------------------
-- global variables --
----------------------
gl_game_play_time_days=0
gl_game_play_time_hours=0
gl_game_play_time_minutes=0
gl_game_time_days=0
gl_game_time_hours=7
gl_game_time_minutes=50
gl_game_time_time_mult=0.075

gl_colors_night={transparent=0,close_background=13,main=6,foreground=7}
gl_colors_sunrise={transparent=9,close_background=13,main=1,foreground=0}
gl_colors_sun={transparent=10,close_background=13,main=1,foreground=0}
gl_colors_sunset={transparent=2,close_background=13,main=6,foreground=7}

gl_sun_night_start_hour=20
gl_sun_sunrise_start_hour=7
gl_sun_sun_start_hour=8
gl_sun_sunset_start_hour=19

gl_app_time_deltams=1000.0/30.0
gl_app_time_timeelapsed=0
gl_app_time_hours_elapsed=0

gl_exit_is_in_menu=false
gl_exit_selected_exit=false

gl_dialogs_dialog_tree_strings="ai;-667;-2;you had no right! go away!;;ai;-123;1;no! please! what do you want?!;;bye;2;-1;leave me alone!;;bye;3;0;bye;;bye;4;0;see you around;;bye;5;1;take care;;hi;6;-2;mhm;;hi;7;0;hello;;hi;8;0;hey, you!;;hi;9;1;good to see you;;cmd;11;-1;follow me;exit this valley with me;cmd;12;-1;stay here;;ai;20;0;i will join you there;;ai;22;0;i don't understand;;ai;23;0;i don't understand;;ai;24;0;i don't understand;;ai;25;0;what?;;ai;26;0;repeat that?;;ai;27;0;please, repeat that;;ai;28;0;please, explain;;que;101;1;how is your day?;;ans;102;-1;leave me alone!;;ans;103;0;not your buisness!;;ans;104;0;too tired to do anything;;ans;105;0;it could be better;;ans;106;0;nothing special;;ans;107;0;quite good;;ans;108;0;good, thanks;;ans;109;1;great day, well rested;;que;111;2;do you need help?;;ans;112;-2;leave me alone!;;ans;113;-1;not your buisness!;;ans;114;0;i don't need your help;;ans;115;0;i think i can handle this;;ans;116;0;no needed;;ans;117;0;no, but thanks;;ans;118;0;thanks for asking, no;;ans;119;1; i'm good, thank you;;st;200;1;bridge is destroyed;;que;201;-1;who else lives here?;;ans;202;-2;leave me alone!;;ans;203;-1;not your buisness!;;ans;204;0;i don't know anyone here;;ans;205;0;i think i'm alone here;;ans;206;0;i only know %ai%;;ans;207;0;I know %ai% and %ai%;;ans;208;0;%ai%, %ai% ;;ans;209;1;%ai%, %ai% and maggus;;st;210;2;my name is...;;que;211;0;what is your name?;;ans;212;-1;leave me alone!;;ans;213;0;not your buisness!;;ans;214;0;and yours is?;;ans;215;0;...;;ans;216;0;...;;ans;217;0;my name is...;;ans;218;1;my name is...;;ans;219;1;my name is...;;que;1021;-3;someone in your house?;;ans;1022;-1;leave me alone!;;ans;1023;0;not your buisness!;;ans;1024;0;no;;ans;1025;0;who knows?;;ans;1026;0;maybe? why?;;ans;1027;0;yes;;ans;1028;0;yes, why?;;ans;1029;1;yes, there is;;que;1031;-6;can I check your house?;;ans;1032;-1;leave me alone!;;ans;1033;0;definetely no!;;ans;1034;0;no;;ans;1035;0;i prefer not;;ans;1036;0;i guess, but quickly!;;ans;1037;0;yes;;ans;1038;0;yes;;ans;1039;1;yes, no need to ask me;;que;1041;0;someone new?;;ans;1042;-1;leave me alone!;;ans;1043;0;not now;;ans;1044;0;no;;ans;1045;0;didn't pay attention;;ans;1046;0;maybe?;;ans;1047;0;yes;;ans;1048;0;yes;;ans;1049;1;yes, name was...;;que;1051;0;where is ...?;;ans;1052;-1;leave me alone!;;ans;1053;0;i will not tell you;;ans;1054;0;i don't know;;ans;1055;0;somewhere around;;ans;1056;0;hm, around...;;ans;1057;0;hm, around...;;ans;1058;0;hm, around...;;ans;1059;1;hm, around...;;st;2000;1;came here today;;st;2010;2;i'm looking for a mage;;st;2020;2;i'm bounty hunter;;st;2030;2;nice weather today;;threat;2200;-10;kill you;i will kill you;threat;2201;-10;kill your family;i will kill your family;threat;2202;-10;tell your secret;i will tell your secret;threat;2203;-10;found the diary;i found the diary;threat;2204;-10;guards will find you;i will tell guards about you;threat;2205;-10;destroy your home;i will destroy your home;ai;2300;-1;leave me alone!;;ai;2301;-1;leave me alone!;;ai;2302;-1;you hear me, leave me alone!;;ai;2303;-1;last time, leave me alone!;;ai;2304;-1;...;;ai;2305;-1;...;;ai;2306;-1;...;;ai;2307;-1;...;;ai;2308;-1;...;;ai;2309;-1;...;;ai;2310;0;mhm;;ai;2311;0;mhm;;ai;2312;0;mhm;;ai;2313;0;i see;;ai;2314;0;now i know;;ai;2315;0;good to know;;ai;2316;0;good to know;;ai;2317;0;thanks for telling me;;ai;2318;0;thanks for telling me;;ai;2319;0;thank you for saying;;"
gl_dialogs_dialog_tree_table={}
gl_dialogs_selection1=1
gl_dialogs_selection2=1
gl_dialogs_selection3=1
gl_dialogs_second_selection=false
gl_dialogs_is_in_menu=false

gl_input_xaxis={raw_value=0.0, down=false, up=false}
gl_input_yaxis={raw_value=0.0,last_raw_value=0.0, down=false, up=false}
gl_input_abutton={raw_value=0.0, down=false, up=false}
gl_input_bbutton={raw_value=0.0, down=false, up=false}

gl_game_results_got_maggus=false
gl_game_results_got_on_time=false
gl_game_results_got_out_alive=false
gl_game_results_threats=0
gl_game_results_angered_npcs=0
gl_dont_give_up_screen=false

gl_camera_x=0
gl_camera_tile_x=0
gl_camera_x_player_offset=64
gl_camera_x_player_max_offset=30
gl_camera_x_current_offset=0
gl_camera_y_player_max_offset=0
gl_camera_y_player_offset=80
gl_camera_y_current_offset=0
gl_camera_y=0
gl_camera_tile_y=0
gl_camera_w=128
gl_camera_h=128
gl_lx=0
gl_ly=0

gl_game_pause_actors=false
gl_game_has_player=false
gl_game_unique_id=1

gl_farmer_spawned=false
gl_royal_spawned=false

gl_cinematic_turned_on=false
gl_cinematic_time=0

gl_game_started=false

gl_maggus_fake_name="maggus"
gl_recluse_name=nil

gl_map_grass_templates={
  192,193,194,195,
  208,209,210,211,
}
gl_map_rocky_grass_templates={
  132,133,134,135,
  136,137,138      
}

gl_play_music=true

gl_selected_searching_house=false
gl_searching_house_dialog=false
gl_house_search_without_perm=0

gl_names="alys;merek;anastas;ulric;tybalt;alianor;carac;borin;millicent;ayleth;"
gl_variable_init_table={"key","type"}
gl_not_used_names={}
gl_current_colors={}
gl_previous_colors=gl_colors_night
gl_current_part_of_day=gl_sun_night_start_hour
gl_objects={}
gl_actors={}
gl_ais={}
gl_ai_names={}
gl_player={}
gl_maggus={}
gl_maggus_corpse=nil
gl_clouds={}
gl_particles={}
gl_dead_particles={}
gl_close_background_objects={}
gl_foreground_objects={}
gl_maggus_spawn_points={}


----------------------
-- pico8 functions --
----------------------
function menu_music()
    gl_play_music=not gl_play_music
    music(gl_play_music and 1 or -1,0,7) 
    menuitem(2,"turn music"..(gl_play_music and " off" or " on"), menu_music)
end
function _init()
  --title_screen()
  init_main()
  menuitem(2,"turn music off", menu_music)
  menuitem(3,"show controls", function() gl_game_started=false end)
end
function _update()
  if not gl_game_started then
    if btn(4) or btn(5) then
      gl_game_started=true
      init_game()
      return
    end
  end
  update_game()
  if gl_game_has_player then
    gl_camera_x=min(max(0,gl_player.x-gl_camera_x_player_offset+gl_camera_x_current_offset),896)
    gl_camera_y=min(gl_player.y-gl_camera_y_player_offset+gl_camera_y_current_offset,128)
    gl_camera_tile_x=pixel_tile_conv(gl_camera_x)
    gl_camera_tile_y=pixel_tile_conv(gl_camera_y)
    gl_lx=gl_camera_x-tile_pixel_conv(gl_camera_tile_x)
    gl_ly=gl_camera_y-tile_pixel_conv(gl_camera_tile_y)
      --doing those calculation on _draw would slow the game
    update_calculate_close_background()
    update_calculate_foreground()
  end
end
function _draw()
  if gl_game_started then
    draw_game()
  else
    draw_intro_screen()
  end
end

----------------------
--  init functions  --
----------------------

function init_main()
  init_dialog_system()
  init_clouds()
  init_grass()
  init_bridge_on_background_fix()

end
function init_bridge_on_background_fix()
    local bridge_sprites="64;65;66;67;68;69;70;71;80;81;82;83;84;85;86;87;"
    local bridge_sprites_working="224;225;226;227;228;229;196;197;240;241;242;243;244;245;212;213;"
    --logger_debug("bfixt1:"..#table..";"..#table2)
    for i=1,125 do
      for j=0,31 do
        local contains, key=table_contains(explode_internal(bridge_sprites),mget(i,j).."")
        if contains then 
          local obj={}
          cl_object_init(obj,tile_pixel_conv(i),tile_pixel_conv(j))
          obj.spr=252
          add(gl_close_background_objects,obj)

          mset(i,j,explode_internal(bridge_sprites_working)[key]+0)
        end
      end
    end
end
function init_grass()
    for i=-1,125 do
      for j=-3,29 do
        local el = {}
        local obj=gl_map_grass_templates[flr(rnd(#gl_map_grass_templates))+1]
        local collision_tile=mget(i,29-j)
        local collision_tile_down=mget(i,29-j+1)
        if(j>10 and rnd(10)<4) obj=gl_map_rocky_grass_templates[flr(rnd(#gl_map_rocky_grass_templates))+1]
        if obj~=nil and (collision_tile~=253 and collision_tile~=230) and (collision_tile_down==253 or collision_tile_down==230) and (collision_tile<128 or collision_tile>131) and collision_tile~=89 and collision_tile~=112 then
          if (collision_tile==1 or (i==8 and 29-j==4)) obj=gl_map_rocky_grass_templates[2]
          el.spr=obj
          local terrain_size=15+tile_pixel_conv(j)           
          if not (i>3 and i<7 and j==25) then
            el.x=tile_pixel_conv(i)
            el.y=128-tile_pixel_conv(el.h or 1)-terrain_size
            el.flip_x=rnd(10)<4
            add(gl_foreground_objects,el)    
          end
        end
      end
    end
end


function init_clouds()
  for i=0,8 do
    add(gl_clouds,{
      x=rnd(128),
      y=rnd(20),
      spd=1+rnd(4),
      w=32+rnd(32)
    })
  end
  for i=0,24 do
    add(gl_particles,{
      x=rnd(128),
      y=rnd(128),
      s=0+flr(rnd(5)/4),
      spd=0.25+rnd(5),
      off=rnd(1),
      c=6+flr(0.5+rnd(1))
    })
  end
end
function init_game()
  gl_game_has_player=false

  init_player()
  init_npcs()
end
function init_dialog_system()
    gl_dialogs_dialog_tree_table=explode_internal(gl_dialogs_dialog_tree_strings,explode_internal("id;topic_id;effect;msg;msg_long;"))
    foreach(gl_dialogs_dialog_tree_table,function(dialog)
      dialog.is_a_threat_or_a_command=(dialog.id=="cmd" or dialog.id=="threat") and true or false
      end)
    gl_not_used_names=explode_internal(gl_names)
end


function init_player()
  if not gl_game_has_player then
    local new_actor={}
    gl_game_has_player=true
    cl_player_init(new_actor,32,32)
    gl_player=new_actor
    --else
    --  logger_debug("there is no player tile (1) on map!")
  end
end
function init_npcs()
  local farmer={}
  local royal={}

  if(rnd(1)<0.85) cl_farmer_ai_init(farmer,tile_pixel_conv(19),tile_pixel_conv(29))
  if(rnd(1)<0.75) cl_royal_ai_init(royal,tile_pixel_conv(114),tile_pixel_conv(29))

  local mercenary={}
  cl_mercenary_ai_init(mercenary,64,32)

  local recluse_spawn_points={}
  for i=0,127 do
    for j=0,31 do
      if(mget(i,j)==63) add(gl_maggus_spawn_points,{x=i,y=j})
      if(mget(i,j)==61) add(recluse_spawn_points,{x=i,y=j})
    end
  end
  gl_maggus={}   

    local recluse=recluse_spawn_points[flr(rnd(#recluse_spawn_points-1))+1]
    del(recluse_spawn_points,recluse)
    cl_recluse_ai_init({},tile_pixel_conv(recluse.x),tile_pixel_conv(recluse.y))

  -- maggus spawns last
  local spawn_point=gl_maggus_spawn_points[flr(rnd(#gl_maggus_spawn_points-1))+1]
  local recluse=recluse_spawn_points[1]
  cl_maggus_init(gl_maggus,tile_pixel_conv(spawn_point.x),tile_pixel_conv(spawn_point.y),tile_pixel_conv(recluse.x),tile_pixel_conv(recluse.y))

end


----------------------
-- update methods --
----------------------

function update_game()
  if not gl_game_has_player and (btnp(5) or btnp(4)) then
    if not gl_dont_give_up_screen and not gl_game_results_got_maggus then
      gl_dont_give_up_screen=true
    else
      run()
      return
    end
  end
  if (not gl_game_started) return
  gl_app_time_timeelapsed+=gl_app_time_deltams/1000.0
  if gl_app_time_timeelapsed>=3600 then
    gl_app_time_timeelapsed-=3600
    gl_app_time_hours_elapsed+=1
  end
  update_game_time()
  update_input()

  update_objects()


  gl_debug_stat_update=stat(1)
end
function update_play_time() -- difficult to optimize this and game_time
  local minutes=gl_game_play_time_minutes+(gl_app_time_deltams/33)*gl_game_time_time_mult -- to game whole day in game take >10 minutes in irl
  if minutes>60 then
    minutes=0
    gl_game_play_time_hours+=1
  end
  if gl_game_play_time_hours>=24 then
    gl_game_play_time_days+=1
    gl_game_play_time_hours=0
  end  
  gl_game_play_time_minutes=minutes
end
function update_game_time()
  if(not gl_game_has_player) return
  local minutes=gl_game_time_minutes+(gl_app_time_deltams/33)*gl_game_time_time_mult -- to game whole day in game take >10 minutes in irl
  update_play_time()
  if minutes>60 then
    minutes=0
    gl_game_time_hours+=1
  end
  if gl_game_time_hours>=24 then
    gl_game_time_days+=1
    gl_game_time_hours=0
  end
  if gl_game_time_hours>=gl_sun_night_start_hour or gl_game_time_hours<gl_sun_sunrise_start_hour then
    gl_previous_colors=gl_colors_sunset
    gl_current_colors=gl_colors_night
    gl_current_part_of_day=gl_sun_night_start_hour
  end
  if gl_game_time_hours>=gl_sun_sunrise_start_hour and gl_game_time_hours<gl_sun_sun_start_hour then
    gl_previous_colors=gl_colors_night
    gl_current_colors=gl_colors_sunrise
    gl_current_part_of_day=gl_sun_sunrise_start_hour
  end
  if gl_game_time_hours>=gl_sun_sun_start_hour and gl_game_time_hours<gl_sun_sunset_start_hour then
    gl_previous_colors=gl_colors_sunrise
    gl_current_colors=gl_colors_sun
    gl_current_part_of_day=gl_sun_sun_start_hour
  end
  if gl_game_time_hours>=gl_sun_sunset_start_hour and gl_game_time_hours<gl_sun_night_start_hour then
    gl_previous_colors=gl_colors_sun
    gl_current_colors=gl_colors_sunset
    gl_current_part_of_day=gl_sun_sunset_start_hour
  end
  gl_game_time_minutes=minutes
end
function update_input()
  local lastx=gl_input_xaxis.raw_value
  local lasty=gl_input_yaxis.raw_value
  local lasta=gl_input_abutton.raw_value
  local lastb=gl_input_bbutton.raw_value

  local xaxis = btn(0) and -1.0 or (btn(1) and 1.0 or 0.0) 
  local yaxis = btn(2) and -1.0 or (btn(3) and 1.0 or 0.0) 
  local abutton= btn(5) and 1.0 or 0.0
  local bbutton= btn(4) and 1.0 or 0.0

  gl_input_xaxis.raw_value=xaxis
  gl_input_xaxis.up=xaxis==0 and lastx!=0
  gl_input_xaxis.down=xaxis!=0
  gl_input_yaxis.raw_value=yaxis
  gl_input_yaxis.up=yaxis==0 and lasty!=0
  gl_input_yaxis.down=yaxis!=0
  gl_input_abutton.raw_value=abutton
  gl_input_abutton.up=abutton==0 and lasta!=0
  gl_input_abutton.down=abutton!=0
  gl_input_bbutton.raw_value=bbutton
  gl_input_bbutton.up=bbutton==0 and lastb!=0
  gl_input_bbutton.down=bbutton!=0

  gl_input_yaxis.last_raw_value=lasty
end
function update_objects()
  foreach(gl_objects,function(obj)
    cl_object_move(obj,obj.spd_x,obj.spd_y)
      obj.update(obj) 
  end)
end
function update_calculate_close_background()
  foreach(gl_close_background_objects, function(this)
    this.local_x=this.x-gl_camera_x
    this.local_y=this.y-gl_camera_y
    this.local_x2=this.local_x+8
    this.local_y2=this.local_y+8
    this.draw_in_this_frame= this.local_x<128 and this.local_y<128 and this.local_x2>0 and this.local_y2>0
  end)
end
function update_calculate_foreground()
  foreach(gl_foreground_objects, function(this)
    this.local_x=this.x-gl_camera_x
    this.local_y=128-gl_camera_y+this.y
    this.draw_in_this_frame=this.local_x+8>0 and this.local_y+8>0 and this.local_x<128 and this.local_y<128
  end)
end
----------------------
--   draw methods   --
----------------------

function draw_game()
    draw_reset_screen()
    if not gl_game_has_player then
      pal()
      palt()    
      draw_game_over_screen()
      if(gl_dont_give_up_screen) draw_dont_give_up_screen()
      return
    end
    draw_clouds()

    draw_close_background()

    draw_terrain()
    draw_objects()
    draw_foreground()
    --for gui
    pal()
    palt()
    cl_player_draw_dialog_menu(gl_player)
    draw_cinematic_mode()
    draw_actors_dialog()
    gl_debug_stat_render=stat(1)
    draw_exit_menu()
    draw_search_house_dialog()
    --draw_debug()
end
function draw_text_screen_helper(text_string)
  local y=1

  rectfill(0,0,128,128,0)
  foreach(explode_internal(text_string,{"m","c","s"}),function(obj)
      print(obj.m,0,y,obj.c)
      y+=obj.s
    end)
end
function draw_intro_screen()
  draw_text_screen_helper("pico8 controls (keyboard);10;16; (directional arrows);11;8;movement;9;12; (z);11;8;conversation menu/cancel;9;12; (x);11;8;search house/confirm;9;12;pause (enter);11;8;general options;9;16;press / to continue;8;8;save yourself v1.0c by;7;8;dagondev, copyright 2016;7;8;")
end
function draw_dont_give_up_screen()
  draw_text_screen_helper("don't give up!;10;30;this game was meant to be played\nseveral times over;9;16;game has several variables that are\nrandomly generated, \nnext game may be different!;9;24;explore world, observe details,\nand enjoy yourself;9;24;good luck next time!;11;9;")
end
function draw_game_over_screen()
  local text_string="game over;10;16;summary:;10;8; ;10;8;"
  local score=0
  if gl_game_results_got_on_time then
    text_string=text_string.."+015| got back on time;11;8;"
    score+=15
  end
  if gl_game_results_got_maggus then  
    text_string=text_string.."+100| brought back the mage;11;8;"
    score+=100
  elseif gl_game_results_got_out_alive then
    text_string=text_string.."-025| abandoned the hunt;8;8;"
    score-=25
  end
  if gl_game_results_threats==0 then
    text_string=text_string.."+020| didn't threaten anyone;11;8;"
    score+=20
  else
    text_string=text_string.."-0"..(gl_game_results_threats<10 and "0" or "")..gl_game_results_threats.."| used threat "..gl_game_results_threats.." times;8;8;"
    score-=gl_game_results_threats
  end
  if gl_game_results_angered_npcs>0 then    
    text_string=text_string.."-0"..(gl_game_results_angered_npcs<10 and "0" or "")..gl_game_results_angered_npcs.."| "..gl_game_results_angered_npcs.." angered npcs;8;8;"
    score-=gl_game_results_angered_npcs
  else
    text_string=text_string.."+020| didn't angered anyone;11;8;"
    score+=20
  end
  if gl_house_search_without_perm>0 then
    text_string=text_string.."-0"..(gl_house_search_without_perm<10 and "0" or "")..gl_house_search_without_perm.."| searched house without permission "..gl_house_search_without_perm.." times;8;8;"
    score-=gl_house_search_without_perm
  end
  if gl_game_results_got_out_alive then
    if not gl_game_results_got_on_time then
      if gl_game_time_days>2 then
          text_string=text_string.."-020| didn't get back on time;8;8;"
          score-=20
      end
    end
  else
    text_string=text_string.."-100| died by falling;8;8;"
    score-=100
  end
    text_string=text_string.." ;10;8;your score is: "..score..";10;8;game time: "..gl_game_play_time_days.."D "..gl_game_play_time_hours.."H "..flr(gl_game_play_time_minutes).."M;9;8;"--"real time:;10;8;"..gl_app_time_hours_elapsed..":"..(gl_app_time_timeelapsed/60)..":"..(gl_app_time_timeelapsed%60)..";9;8;"

  draw_text_screen_helper(text_string)
  --logger_debug((gl_game_results_got_on_time and 1 or 0)..(gl_game_results_encountered_maggus and 1 or 0)..(gl_game_results_recognize_maggus and 1 or 0)..(gl_game_results_got_maggus and 1 or 0)..(gl_game_results_maggus_alive and 1 or 0)..(gl_game_results_maggus_dead and 1 or 0)..(gl_game_results_got_out_alive and 1 or 0))
end

function draw_reset_screen()
  pal()
  palt()
  pal(gl_colors_night.transparent,gl_current_colors.transparent) 
  pal(gl_colors_night.close_background,gl_current_colors.close_background)
  pal(gl_colors_night.main,gl_current_colors.main)
  pal(gl_colors_night.foreground,gl_current_colors.foreground)

  rectfill(0,0,128,128,gl_colors_night.transparent)

  --transition between phases of day
  local time_of_day_delta=abs(gl_game_time_hours*60+gl_game_time_minutes-gl_current_part_of_day*60)*2.5
  if(time_of_day_delta>160) return
  pal(gl_colors_night.transparent,gl_previous_colors.transparent)
  local y=127-time_of_day_delta
  rectfill(0,0,128,y,0)

  line(0,y+2,128,y+2,0)
  line(0,y+3,128,y+3,0)  
  line(0,y+5,128,y+5,0)
  line(0,y+9,128,y+9,0)
  line(0,y+17,128,y+17,0)

  pal(gl_colors_night.transparent,gl_current_colors.transparent)
  y=128-time_of_day_delta-4
  line(0,y,128,y,0)

  pal(0,0)
end

function draw_clouds()
  local delta=-((gl_camera_y+60)/5)
  foreach(gl_clouds, function(c)
    c.x += c.spd
    rectfill(c.x,c.y+delta,c.x+c.w,c.y+4+(1-c.w/64)*12+delta,new_bg~=nil and 14 or 1)
    if c.x > 128 then
      c.x = -c.w
      c.y=rnd(28)
    end
  end)
    -- snow
  foreach(gl_particles, function(p)
    p.x += p.spd
    p.y += sin(p.off)
    p.off+= min(0.05,p.spd/32)
    rectfill(p.x,p.y,p.x+p.s,p.y+p.s,p.c)
    if p.x>128+4 then 
      p.x=-4
      p.y=rnd(128)
    end
  end)

  foreach(gl_dead_particles, function(p)
    p.x += p.spd_x
    p.y += p.spd_y
    p.t -=1
    if p.t <= 0 then del(gl_dead_particles,p) end
    rectfill(p.x-p.t/5,p.y-p.t/5,p.x+p.t/5,p.y+p.t/5,14+p.t%2)
  end)
end
function draw_terrain()

  local ydelta=17
  map(gl_camera_tile_x,gl_camera_tile_y,-gl_lx,-gl_ly,17,ydelta,1)
end
function draw_objects()
  foreach(gl_objects, function(o)
      o.draw(o)
  end)
end
function draw_close_background()
  foreach(gl_close_background_objects, function(this)
    if(this.draw_in_this_frame) rectfill(this.local_x,this.local_y,this.local_x2,this.local_y2,13)
  end)

  map(gl_camera_tile_x,gl_camera_tile_y,-gl_lx,-gl_ly,17,17,32)
end
function draw_foreground()
  foreach(gl_foreground_objects, function(this)
    if (this.draw_in_this_frame) spr(this.spr,this.local_x,this.local_y,1,1,this.flip_x,this.flip_y)
  end)
  map(gl_camera_tile_x,gl_camera_tile_y,-gl_lx,-gl_ly,17,17,128)
end
function draw_cinematic_mode()
  if(not gl_cinematic_turned_on and gl_cinematic_time==0) return   
  
  local delta_time=gl_app_time_timeelapsed-gl_cinematic_start_time
  local bar_height=128
  if delta_time>=3 or not gl_cinematic_turned_on or gl_cinematic_skipped then
    if gl_cinematic_turned_on or gl_cinematic_skipped then
      bar_height=max(gl_cinematic_skipped and 0 or 15,128-gl_cinematic_time)
    else
      bar_height=max(0,15-gl_cinematic_time)
    end
    gl_cinematic_time+=1
  end
    if(bar_height==0) gl_cinematic_time=0
  palt(0,false)
  rectfill(0,128-bar_height,128,128,0) 
  rectfill(0,0,128,bar_height,0) 
  palt()  
end
function draw_actors_dialog()
  local s=0
  foreach(gl_actors, function(o)
    s-=8
    cl_actor_draw_say(o,gl_camera_x,gl_camera_y-s)
  end)
end
function draw_dialog(desc,selected_yes)
  rectfill(15,30,112,80,0)  
  rect(15,30,112,80,9)
  local sel_x=selected_yes and 45 or 75
  print(desc,46-2-#desc/2,42,11)
  print("no",49,62,11)
  print("yes",78,62,8)
  rect(sel_x,60,sel_x+15,70,8)
end
function draw_exit_menu()
  if(not gl_exit_is_in_menu) return
  draw_dialog("do you want to leave\nthis place for ever?",gl_exit_selected_exit)
end
function draw_search_house_dialog()
  if(not gl_searching_house_dialog) return
  draw_dialog("do you want to \nsearch this house?",not gl_selected_searching_house)
end
----------------------
-- collision methods --
----------------------

function collision_tile_flag_at(x,y,w,h,flag)
  for i=max(0,pixel_tile_conv(x)),min(127,pixel_tile_conv(x+w-1)) do
    for j=max(0,pixel_tile_conv(y)),min(31,pixel_tile_conv(y+h-1)) do
      if fget(mget(i,j),flag) then
        return true
      end
    end
  end
  return false
end

----------------------
-- basic methods (which could be in pico8 itself - thus no prefix) --
----------------------


function appr(val,target,amount)
  return val > target and max(val - amount, target) 
  or min(val + amount, target)
end
function sign(v)
  return v>0 and 1 or v<0 and -1 or 0
end
function maybe()
  return rnd(1)<0.5
end
function copy_table(table)
  if(table==nil) return nil
  local out_table={}
  for key,val in pairs(table) do
    out_table[key]=val
  end  
  return out_table
end
function table_contains(table, value)
  for key,val in pairs(table) do
    if(val==value) return true,key
  end  
  return false,nil
end
function pixel_tile_conv(val) return flr(val/8) end
function tile_pixel_conv(val) return val*8 end
function explode_internal(s,id_table,data_table)
  local delimiter=";"
  if(data_table==nil) data_table={}
  if(id_table==nil) id_table={}
  local n_max=#id_table
  local val_table={}
  local lastpos = 1
  local n=1
  for i=1,#s do
    if sub(s,i,i) == delimiter then
      local id=id_table[n]
      local new_pos=i-1
      local val=sub(s, lastpos, new_pos)
      --logger_debug(id..";"..lastpos..";"..new_pos..";"..val..(lastpos<=new_pos and ";adding" or ";nil"))
      i += 1
      n+=1
      if n_max==0 then
        add(data_table,val)
      else
        if(lastpos<=new_pos) val_table[id]=val
        if n>n_max then
          n=1
          add(data_table,val_table)
          val_table={}
        end
      end
      lastpos = i
    end
  end
  return data_table
end

----------------------
-- cutscene methods --
----------------------

function cinematic_turn_on()
  if(gl_cinematic_turned_on) return
  gl_cinematic_turned_on=true
  gl_cinematic_time=1
  gl_cinematic_start_time=gl_app_time_timeelapsed
  sfx(31+flr(rnd(2)),3)
end
function cinematic_turn_off(skipped)
  if(not gl_cinematic_turned_on) return
  gl_cinematic_turned_on=false
  gl_cinematic_time=1
  gl_cinematic_skipped=skipped
   music(0,0,7)
end

----------------------
-- game 'classes' --
----------------------
function cl_generate_default_values_for_ids_from_table(this,id_type_table)
  foreach(id_type_table,function(value)
    local t=value.type
    local key=value.key
    this[key]=false
    if(t=="int") this[key]=0
    end)
end
function cl_object_init(this,x,y)
  cl_generate_default_values_for_ids_from_table(this,explode_internal("flip_x;bool;flip_y;bool;hitbox_x;int;hitbox_y;int;spd_x;int;spd_y;int;rem_x;int;rem_y;int;local_x;int;local_y;int;local_x2;int;local_y2;int;draw_in_this_frame;bool;",gl_variable_init_table))
 
  this.x = x
  this.y = y

  this.hitbox_w=8
  this.hitbox_h=8


  this.draw=cl_object_draw
  this.move_y=cl_object_move_y

end
function cl_object_destroy(this)
    del(gl_objects,this)
end

function cl_object_is_solid(this,ox,oy)
  local x=this.x+this.hitbox_x+ox
  local y=this.y+this.hitbox_y+oy
  return collision_tile_flag_at(x,y,this.hitbox_w,this.hitbox_h,0) or y>=252 or ((x<0 and y>=36) or x>1024)
end
function cl_object_move(this,ox,oy)
  local amount
  -- [x] get move amount
  this.rem_x += ox
  amount = flr(this.rem_x + 0.5)
  this.rem_x -= amount
  cl_object_move_x(this,amount,0)
  
  -- [y] get move amount
  this.rem_y += oy
  amount = flr(this.rem_y + 0.5)
  this.rem_y -= amount
  this.move_y(this,amount)
end
function cl_object_move_x(this, amount,start)
  local step = sign(amount)
  for i=start,abs(amount) do
    if not cl_object_is_solid(this,step,0) then
      this.x += step
    else
      this.spd_x = 0
      this.rem_x = 0
      break
    end
  end
end
function cl_object_move_y(this, amount)
  local step = sign(amount)
  for i=0,abs(amount) do
    if not cl_object_is_solid(this,0,step) then
      this.y += step
    else
      this.spd_y = 0
      this.rem_y = 0
      break
    end
  end
end
function cl_object_draw(this)
    spr(this.spr,this.x,this.y,1,1,this.flip_x,this.flip_y)
end


function cl_actor_init(this,x,y)
  cl_object_init(this,x,y)
  add(gl_objects,this)

  cl_generate_default_values_for_ids_from_table(this,explode_internal("spr;int;is_player;bool;spr_off;int;killed;bool;falling_time;int;anim_state;int;last_anim_state;int;climbing;bool;using_ladder;bool;sitting;bool;is_sitting_up;bool;lying_down;bool;is_waking_up;bool;waked_up;bool;ask_player_to_leave;int;",gl_variable_init_table))

  this.hitbox_x=1
  this.hitbox_y=3
  this.hitbox_w=4
  this.hitbox_h=5

  cl_actor_reset_input(this)
  this.killed=false

  this.maxrun=0.5
  this.maxduckrun=0.2
  this.maxfall=8
  this.safefall=5
  this.deccel=0.15
  this.accel=0.6
  this.dangerous_falling_time=20

  this.message_color=(5+#gl_actors)%15+1
  this.draw_message_queue={}

  this.unique_id=gl_game_unique_id
  gl_game_unique_id+=1
  local new_name=gl_not_used_names[1]
  this.dialog_name=new_name
  del(gl_not_used_names,new_name)

  this.update=cl_actor_update
  this.kill=cl_actor_kill
  this.move_actor=cl_actor_move_actor
  this.move_y=cl_actor_move_y
  this.say=cl_actor_say

  add(gl_actors,this)
end
function cl_actor_reset_input(this)
  this.left = false
  this.right = false
  this.up= false
  this.down= false
  this.abutton=false
  this.bbutton= false 
end
function cl_actor_update(this)
    if (gl_game_pause_actors) return
    if(this.killed) return

    if this.process !=nil then
      this.process(this)
    end

    local input_x = this.right and 1 or (this.left and -1 or 0)
    local input_y =this.up and -1 or (this.down and 1 or 0) 

    this.using_ladder= (cl_actor_is_ladder(this,0,0) and this.up) or (this.down and cl_actor_is_ladder(this,0,8)) 

    local was_climbing=this.climbing
    this.climbing=this.up and not cl_object_is_solid(this,0,-1) and this.falling_time<this.dangerous_falling_time and abs(this.spd_y)<this.safefall and
    ((not this.flip_x and cl_object_is_solid(this,1,0)) or (this.flip_x and cl_object_is_solid(this,-1,0)) ) and not this.using_ladder
    if (was_climbing and not this.climbing) this.spd_y-=0.5

    local on_ground=cl_object_is_solid(this,0,1)

    if on_ground then
      if (this.falling_time!=0) sfx(63,2)
      this.falling_time=0
    else
      this.falling_time+=1
    end

    local gravity=abs(this.spd_y) <= 0.15 and 0.105 or 0.21
    if not on_ground then
      this.spd_y=appr(this.spd_y,this.maxfall,gravity)
      this.accel=0.4
    else 
      this.accel=0.6
    end
    
    local maxspeed=this.down and this.maxduckrun or this.maxrun

    this.move_actor(this,input_x,input_y,maxspeed)

    cl_actor_animation(this,input_x,on_ground)

    -- was on the ground
    this.was_on_ground=on_ground
end
function cl_actor_draw(this)
  spr(this.spr,this.x,this.y,1,1,this.flip_x,this.flip_y)   
end

function cl_actor_draw_say(this,x,y)
  x=this.x-x
  y=this.y-y
  
  palt(0,false)
  foreach(this.draw_message_queue,function(obj)
      local deltatime=gl_app_time_timeelapsed-obj.time
      local text_size=#obj.m*2
      local text_time=text_size/10.0+1
      local x1=x-text_size-2
      local x2=x+text_size+2
      if x1<0 and x2>128 then

      else
        while(x2>128) do
          x1-=1
          x2=x1+text_size*2
        end
        while(x1<0) do
          x1+=1
          x2+=1
        end
      end
      if deltatime<text_time then
        if obj.not_played_sfx then
          obj.not_played_sfx=false
          sfx(obj.sound,2)
        end
        rectfill(x1-6,y-12,x2,y-4,0)
        print(obj.m,x1,y-10,obj.color)
      end
    end)
    palt()
end
function cl_actor_kill(this)
  --sfx(0)
  this.killed=true

  del(gl_actors,this)
  cl_object_destroy(this)  
end
function cl_actor_animation(this,input_x, on_ground)
    local animation_string="lying_down;17;1;-2;1;waking_up;17;7;-1;1;climbing;11;6;0;1;using_ladder;28;4;1;1;falling;6;4;2;1;ducking;4;1;3;1;standing;0;1;4;1;sitting;32;5;5;1;moving;0;4;6;1;sit;35;6;7;0.1;"
    local animation_table=explode_internal(animation_string,explode_internal("id;sprite;offset;anim_state;speed_mult;"))
    local sprite=0
    local offset=1
    local index=0
    local speed_mult=1
    if this.lying_down then
      index=1
    elseif this.is_waking_up and not this.waked_up then
      index=2
    elseif this.climbing then  
      index=3
      this.falling_time=0
    elseif this.using_ladder then
      index=4
      this.falling_time=0
    elseif not on_ground and this.falling_time>this.dangerous_falling_time then --falling
      index=5
    elseif this.down and not this.is_attacking then --ducking
      index=6    
      if(this.left or this.right) offset=2
    elseif this.sitting and not this.is_sitting_up then
      index=8
    elseif this.is_sitting_up then
      index=10
    elseif this.is_attacking then
      index=13
    elseif this.spd_x==0 or (not this.left and not this.right) then --standing
      index= 7
    else --moving
      index=9
    end
    if index>0 then
      local t=animation_table[index]
      sprite=t.sprite
      if(offset==1) offset=t.offset --allowing to change offset by code for special cases like in ducking
      anim_state=t.anim_state
      speed_mult=t.speed_mult
    end
    if (this.last_anim_state~=this.anim_state) this.spr_off=0
    this.last_anim_state=this.anim_state
    this.spr=sprite+(this.spr_off*speed_mult)%offset
    this.spr_off+=0.25
    if index==2 then
      if this.spr_off%offset==0 and this.last_anim_state==this.anim_state then
        this.waked_up=true
        return
      end
    end
    if index==8 then
      if this.spr_off%offset==0 and this.last_anim_state==this.anim_state then
        this.is_sitting_up=true
        return
      end
    end
end
function cl_actor_move_actor(this,input_x, input_y, maxspeed)
  if this.climbing or this.using_ladder then
    this.spd_y=appr(this.spd_y,input_y*maxspeed/1.4,this.accel/1.5)
  end

  local a=this.spd_x
  local b=abs(this.spd_x) > maxspeed and sign(this.spd_x)*maxspeed or input_x*maxspeed
  local c=this.accel
  this.spd_x=appr(a,b,c)
  
  if this.spd_x!=0 then
    local flipcur=this.spd_x<0
    --slows movement when flipping
    if flipcur!=this.flip_x then
      this.spd_x=0
    end
    this.flip_x=flipcur
  end
end
function cl_actor_move_y(this,amount)
  local step = sign(amount)
  for i=0,abs(amount) do
    if not cl_object_is_solid(this,0,step) or (this.using_ladder and cl_actor_is_ladder(this,0,step)) then
      this.y += step
    else
      if (amount==this.maxfall) this.kill(this)
      this.spd_y = 0
      this.rem_y = 0
      break
    end
  end
end
function cl_actor_is_ladder(this,ox,oy)
  return collision_tile_flag_at(this.x+this.hitbox_x+1+ox,this.y+this.hitbox_y+oy,this.hitbox_w-1,this.hitbox_h,2)
end
function cl_actor_say(this,message)
  local msg=copy_table(message)
  local topic_id_int=(msg.topic_id or 0)+0
  if topic_id_int==210 or topic_id_int>214 and topic_id_int<220 then 
    cl_actor_pass_arg_to_dialog(msg,this.dialog_name,false)
    if(this.unique_id!=gl_player.unique_id and not table_contains(gl_player.known_names,this.dialog_name)) add(gl_player.known_names,this.dialog_name)  
  end
  local text=msg.msg_long~=nil and msg.msg_long or msg.msg
  if(#this.draw_message_queue>0) this.draw_message_queue={}
  local sound_offset=(this.unique_id-1)%3
  add(this.draw_message_queue,{m=text,color=this.message_color,time=gl_app_time_timeelapsed,not_played_sfx=true,sound=(#text>14 and (maybe() and 20 or 23)+sound_offset or (#text>9 and 17+sound_offset or 14+sound_offset))})  
  foreach(gl_ais,function(ai)
    local sayx=this.x
    local sayy=this.y
    local maxx=ai.is_in_house and 0 or (msg.is_a_threat_or_a_command and 24 or 64)
    local maxy=ai.is_in_house and 0 or (msg.is_a_threat_or_a_command and 4 or 64)

    msg.saying_actor=this 

    local ignore = ai.is_in_house or ai.is_asleep or this~=gl_player or (msg.directed_to_id~=nil and msg.directed_to_id~=ai.unique_id)
    if not ignore and ai.unique_id!=this.unique_id and ai.x>sayx-maxx and ai.x<sayx+maxx and ai.y>sayy-maxy and ai.y<sayy+maxy then
        --logger_debug(text..";goes to:"..ai.dialog_name)
      msg.flip=ai.x>this.x
      add(ai.heard_message_queue,msg)
    end
  end)
end
function cl_actor_pass_arg_to_dialog(dialog,to_insert,is_question)
  dialog.msg=sub(dialog.msg,0,#dialog.msg-(is_question and 4 or 3)).." "..to_insert..(is_question and "?" or "")
end
function cl_actor_find_doors(this,range)
  local r= range or 5
    for i=-r,r do
      local x=this.x+tile_pixel_conv(i)
      local y=this.y
      if(fget(mget(pixel_tile_conv(x),pixel_tile_conv(y)),4)) return x,y
    end
  return nil,nil
end

function cl_player_init(this,x,y)
  cl_actor_init(this,x,y)

  this.is_player=true

  this.process=cl_player_process
  this.update=cl_player_update
  this.draw=cl_player_draw
  this.kill=cl_player_kill

  this.asking_about_someone=false
  cl_player_generate_dialog_tables(this)
end
function cl_player_generate_dialog_tables(this)
  gl_dialogs_general_dialog_tree={}
  local help_table={}
  local full_id_table=explode_internal("greet/bye;greet/bye;question;statement;command;threat;",explode_internal("hi;bye;que;st;cmd;threat;"))
  local help_id_table={"question","statement","greet/bye","threat","command"}
  foreach(gl_dialogs_dialog_tree_table,function(option)
    local id=full_id_table[1][option.id]
    if id~=nil then
      local id_table=help_table[id] or {id=id}
      if(id_table.values==nil)id_table.values={}
      add(id_table.values,option)
      help_table[id]=id_table
    end
    end)

  for i=1,#help_id_table do
    for key,val in pairs(help_table) do
      if(val.id==help_id_table[i]) add(gl_dialogs_general_dialog_tree,val) --to make sure order is the same every game
    end  
  end
  this.known_names={"maggus"}
end
function cl_player_draw(this,time_of_day_delta)
  local x=this.x-gl_camera_x-4+(this.hitbox_w-this.hitbox_x)
  local y=this.y-gl_camera_y-2+(this.hitbox_h-this.hitbox_y)

  local below=mget(pixel_tile_conv(this.x+this.hitbox_x),pixel_tile_conv(this.y+this.hitbox_y)+1)
  if(below==203 or below==201 or below==96 or below==99) y+=1

  spr(this.spr,x,y,1,1,this.flip_x,this.flip_y)  
end
function cl_player_draw_dialog_menu(this)
  if (not gl_dialogs_is_in_menu) return
  local len=this.asking_about_someone and #this.known_names or #gl_dialogs_general_dialog_tree
  local x1=1
  local y1=1
  local x2=x1+36
  local y2=y1-1
  rectfill(x1-1,y1-1,x2,y1-1+6*len,0)
  local sel1=flr(gl_dialogs_selection1)-1
  local sel2=flr(gl_dialogs_selection2)-1
  local dialog_color=10
  if this.asking_about_someone then
    local sel3=flr(gl_dialogs_selection3)-1
    foreach(this.known_names,function(ai)
      print(ai,x1,y1,dialog_color)
      y1+=6
    end)  
    rect(x1-1,sel3*6,x2,(sel3+1)*6,8)
    return
  end
  foreach(gl_dialogs_general_dialog_tree,function(obj)
    print(obj.id,x1,y1,dialog_color)
    y1+=6
  end)
  y1=1   
  if(not gl_dialogs_second_selection) rect(x1-1,y1-1+sel1*6,x2,y1-1+(sel1+1)*6,8)
  y2+=sel1*6
  local options=gl_dialogs_general_dialog_tree[sel1+1]
  if options~=nil then
    local len2=#options.values
    rectfill(x2+1,y2,127,y2+6*len2,0)
    if gl_dialogs_second_selection then
      if(len2>0) rect(x2+1,y2+sel2*6,127,y2+(sel2+1)*6,8)    
    else
      rect(x2+1,y2,127,y2+1+6*len2,8)
    end    
    local last_id
    foreach(options.values,function(obj)
      if last_id!=obj.id then
        if(last_id~=nil) line(x2+2,y2,126,y2,1)
        last_id=obj.id
      end
        print(obj.msg,x2+2,y2+1,dialog_color)
        y2+=6
    end)
  y1=1        
  end
end

function cl_player_kill(this)
  gl_game_has_player=false
  gl_player_killed=0
  player={} 
  cl_actor_kill(this)
  sfx(61,3)
  music(38,7)
  menuitem(1)
end
function cl_player_process(this)
    if(this.killed) return

    if(this.x<0 or this.x>1016) gl_exit_is_in_menu=true

    if gl_cinematic_turned_on then
      if (gl_input_abutton.up and gl_app_time_timeelapsed>1) cinematic_turn_off(true)
      return
    end

    this.left = gl_input_xaxis.raw_value<0
    this.right = gl_input_xaxis.raw_value>0
    this.up=gl_input_yaxis.raw_value<0
    this.down=gl_input_yaxis.raw_value>0
    this.abutton=gl_input_abutton.up
    this.bbutton=gl_input_bbutton.up

    if this.bbutton and not gl_exit_is_in_menu and not gl_searching_house_dialog then 
      gl_dialogs_is_in_menu=not gl_dialogs_is_in_menu
      this.asking_about_someone=false
    end

    if gl_dialogs_is_in_menu or gl_exit_is_in_menu or gl_searching_house_dialog then
      cl_player_searching_house_update(this)
      cl_player_update_exit_menu(this)
      cl_player_update_dialog_menu(this)
      cl_actor_reset_input(this)
      return
    end
    local x,y=cl_actor_find_doors(this,1)
    if this.abutton and x~=nil then
      gl_searching_house_dialog=true
    end 

    if (this.left) gl_camera_x_current_offset=max(-gl_camera_x_player_max_offset,gl_camera_x_current_offset-(gl_camera_x_current_offset>0 and 1.5 or 0.75))
    if (this.right) gl_camera_x_current_offset=min(gl_camera_x_player_max_offset,gl_camera_x_current_offset+(gl_camera_x_current_offset<0 and 1.5 or 0.75))
    if (this.up) gl_camera_y_current_offset=max(-gl_camera_y_player_max_offset,gl_camera_y_current_offset-(gl_camera_y_current_offset>0 and 2 or 1))
    if (this.down) gl_camera_y_current_offset=min(gl_camera_y_player_max_offset,gl_camera_y_current_offset+(gl_camera_y_current_offset<0 and 2 or 1))
end
function cl_player_update_exit_menu(this)
  if(not gl_exit_is_in_menu) return
  if (gl_input_xaxis.up) gl_exit_selected_exit=not gl_exit_selected_exit
  local exited=this.abutton

  if not gl_exit_selected_exit and exited then
    this.kill(this)

    gl_game_results_got_out_alive=true
    gl_game_results_got_on_time=gl_game_time_days>0 and gl_game_time_days<3
  end

  if this.bbutton or exited then
      gl_exit_is_in_menu=false
      this.x=this.x>100 and 984 or 1
  end
end
function cl_player_handle_rounding_menu_value(val,max)
    return val>max and 1 or val<1 and max or val
end
function cl_player_searching_house_update(this)
  if(not gl_searching_house_dialog) return
  if (gl_input_xaxis.up) gl_selected_searching_house=not gl_selected_searching_house
  local exited=this.abutton
  if gl_selected_searching_house and exited then
    local response=copy_table(gl_dialogs_general_dialog_tree[1].values[1])
    response.topic_id="-666"
    response.msg_long=nil
    local x,y=cl_actor_find_doors(this,1)
    local said=false
    foreach(gl_ais,function(ai)
      if pixel_tile_conv(x)==pixel_tile_conv(ai.x) and pixel_tile_conv(y)==pixel_tile_conv(ai.y) and ai.is_in_house then
        response.msg="get out!"
        this.say(this,response)
        if ai.is_asleep then 
          cl_ai_ask_player_to_leave(ai)
          response.msg="i was sleeping! get out!"
        else
          response.msg="i'm leaving already!"
          ai.is_in_house=false
          ai.has_to_get_out_the_house=true
        end
        ai.say(ai,response)
        said=true
      end
      end)
    if not said then
      response.msg="there is nobody there"
      this.say(this,response)
    end    
  end

  if this.bbutton or exited then
      gl_searching_house_dialog=false
  end
end
function cl_player_update_dialog_menu(this)
  if(not gl_dialogs_is_in_menu) return
    local len1=#gl_dialogs_general_dialog_tree

    local ydelta=gl_input_yaxis.up and gl_input_yaxis.last_raw_value or 0

    if this.asking_about_someone then
      gl_dialogs_selection3+= ydelta
    elseif gl_dialogs_second_selection then
      gl_dialogs_selection2+= ydelta
    else
      gl_dialogs_selection1+=ydelta
    end
    gl_dialogs_selection1=cl_player_handle_rounding_menu_value(gl_dialogs_selection1,len1)

    local option=gl_dialogs_general_dialog_tree[gl_dialogs_selection1]
    local len2=#(option.values) 
    local len3=#this.known_names

    gl_dialogs_selection2=cl_player_handle_rounding_menu_value(gl_dialogs_selection2,len2)
    gl_dialogs_selection3=cl_player_handle_rounding_menu_value(gl_dialogs_selection3,len3)

    if this.abutton and gl_dialogs_second_selection and len2>0 then 
      local selection = copy_table(option.values[gl_dialogs_selection2])
      local topic_id_int=selection.topic_id+0
      if topic_id_int==1011 or topic_id_int==1051 and not this.asking_about_someone then
        this.asking_about_someone=true
      else
        if(this.asking_about_someone) cl_actor_pass_arg_to_dialog(selection,this.known_names[gl_dialogs_selection3],true)
        if(topic_id_int>2199 and topic_id_int<2206) gl_game_results_threats+=1
        this.say(this,selection)
        this.asking_about_someone=false
        gl_dialogs_second_selection=false
      end
    end
    gl_dialogs_second_selection=this.right or (not this.left and gl_dialogs_second_selection)
end
function cl_player_update(this)
  cl_actor_update(this)
  this.lying_down=gl_cinematic_turned_on and gl_app_time_timeelapsed<7
  this.is_waking_up=gl_cinematic_turned_on and gl_app_time_timeelapsed>=7
end
function cl_ai_init(this,x,y)
    cl_actor_init(this,x,y)

    add(gl_ais, this)
    add(gl_ai_names,this.dialog_name)

    this.process=cl_ai_process
    this.draw=cl_ai_draw   
    this.kill=cl_ai_kill

    this.heard_message_queue={} 
    this.say_message_queue={}
    this.relation_table={}
    this.conversation_tables={}
    this.response_table={}

    cl_generate_default_values_for_ids_from_table(this,explode_internal("yelled_at_player_cooldown;int;allowed_player_to_search_house;bool;given_command;bool;is_in_house;bool;is_asleep;bool;own_a_house;bool;time_to_sleep;bool;maggus_staying_in_house;bool;is_terrorized;bool;has_to_get_out_the_house;bool;",gl_variable_init_table))

    foreach(gl_dialogs_dialog_tree_table,function(option)    
      local topic=option.topic_id
      if(topic+0<2000 or topic+0>(2001)) this.response_table[topic]=option
    end)

    cl_maggus_init_mimic_settings(this)
end
function cl_ai_copy_from_response_table(this,topic_id)
  return copy_table(this.response_table[topic_id..""])
end
function cl_ai_get_starting_relation_value_based_on_perks(this,unique_id)
  return this.relation_table[unique_id] or rnd(90)+30
end
function cl_ai_process(this)
    if(this.killed) return
    this.time_to_sleep=gl_game_time_hours<=gl_sun_sunrise_start_hour or gl_game_time_hours>=gl_sun_sunset_start_hour
    if this.has_to_get_out_the_house then
        local dx=this.door_obj_x-this.x
        this.right=dx>-8
        this.has_to_get_out_the_house=dx>-8
    elseif this.time_to_sleep then
      --logger_debug("time to sleep")
      if this.own_a_house then
        if pixel_tile_conv(this.x)==pixel_tile_conv(this.door_obj_x) then
          --logger_debug("in house")
          this.is_in_house=true
          this.is_asleep=true                
          this.left=false
          this.right=false
        else
          --logger_debug("going to h:"..pixel_tile_conv(this.door_obj.x)..";"..pixel_tile_conv(this.door_obj.y))
          local dx=this.door_obj_x-this.x
          this.left=dx<0
          this.right=dx>0
          this.y=this.door_obj_y 
        end
      else
        this.is_asleep=true
        this.lying_down=true
      end
    elseif this.is_asleep then
      this.is_in_house=false
      this.is_asleep=false
      this.lying_down=false

      --logger_debug("awake at:"..pixel_tile_conv(this.x)..";"..pixel_tile_conv(this.y))
    end
    if(this.is_in_house) return
    --logger_debug(this.name..";"..pixel_tile_conv(this.x)..";"..pixel_tile_conv(this.y)..";"..(this.is_in_house and "house" or "awake"))
    foreach(this.heard_message_queue,function(msg) 
      local response = cl_ai_process_message(this,msg) 

      if response~=nil then
        --logger_debug("r:"..response.msg)
        response.wait_for=#msg.msg/5.5+1
        response.flip=msg.flip
        cl_ai_say_later(this,response)
      end
      end)
    this.heard_message_queue={}
    local del_table={}
    foreach(this.say_message_queue,function(msg)
      local t=gl_app_time_timeelapsed

      if msg.timestamp+msg.wait_for<t then
        this.say(this,msg)
        add(del_table,msg)
      end
    end)      
    foreach(del_table,function(msg)
      del(this.say_message_queue,msg)
    end
)    if this.given_command then
      this.left=false
      this.right=false
    end 
end
function cl_ai_process_message(this,msg)
  if this.ask_player_to_leave>3 then
    --todo: be agressive
    local r=cl_ai_copy_from_response_table(this,"1022")
    if(maybe()) r.msg="..."
    this.say(this,r) 
    return
  end
  local len=#msg.msg
  local response=nil
  if(msg.id==nil) return

  local sayer_unique_id=msg.saying_actor.unique_id
  local relation_value=cl_ai_get_starting_relation_value_based_on_perks(this,sayer_unique_id)
  --logger_debug("rl:"..relation_value)
  local conversation_table=this.conversation_tables[sayer_unique_id] or {}
  local topic_id_string=msg.topic_id
  local topic_id_int=topic_id_string+0

  local response_range=1
  local min_response=0
  local response_boost=1+relation_value/10
  local response_boost_3_points=this.is_terrorized and 3 or min(max(response_boost,0),3)
  local response_boost_6_points=this.is_terrorized and 6 or min(max(response_boost,0),6)
  --logger_debug("b3:"..response_boost_3_points..";b6:"..response_boost_6_points)

  if msg.id=="hi" then
    min_response=6
    response_range=this.is_terrorized and 1 or response_boost_3_points
  elseif msg.id=="bye" then
    min_response=2
    response_range=this.is_terrorized and 1 or response_boost_3_points
  elseif msg.id=="st" then
    min_response=2312
    response_range=this.is_terrorized and 1 or response_boost_6_points
  elseif msg.id=="que" then--respond to any other question with default range (2-9)
    min_response=topic_id_int+1
    if topic_id_int==201 then
      response_range=response_boost_3_points*2
    elseif topic_id_int==1021 then
      response_range=this.maggus_staying_in_house and response_boost_6_points or response_boost_3_points
    else
      response_range=response_boost_6_points
    end
  end
  --logger_debug("min:"..min_response..";max:"..max_response)
  add(conversation_table,topic_id_string)
  local max_response=min_response+response_range

  if this.if_there_is_no_response_use_more_negative_one and relation_value<this.liking_actor_breakpoint then
    response=cl_ai_get_topic_from_response_table(this,min_response,max_response,1,topic_id_int,conversation_table)
  else 
    response=cl_ai_get_topic_from_response_table(this,max_response,min_response,-1,topic_id_int,conversation_table)
  end



  if topic_id_int>2199 then 
    if topic_id_int==2200 then 
      response=cl_ai_terrorized(this)
    else
      if (this.is_ai_terrorized(topic_id_int)) then
        return cl_ai_terrorized(this)
      else
        return cl_ai_ask_player_to_leave(this)
      end
    end
  end

  if msg.id=="cmd" and msg.saying_actor.is_player then
    response=cl_ai_copy_from_response_table(this,"1024")   
    if (this.is_terrorized or ((gl_game_time_hours>gl_sun_night_start_hour or gl_game_time_hours<gl_sun_sunrise_start_hour) and not this.is_asleep)) then
      this.given_command=topic_id_int==11 
      response=cl_ai_copy_from_response_table(this,topic_id_int==11 and "20" or "1027")
      if(topic_id_int==11 and this.is_maggus) cl_maggus_init_mimic_settings(this)
    else
      response=cl_ai_ask_player_to_leave(this,topic_id_string)
    end
  end 
  if topic_id_string=="-666" and not this.is_terrorized and not this.allowed_player_to_search_house and not this.is_maggus then
   response=cl_ai_copy_from_response_table(this,"-667")  
   gl_house_search_without_perm+=1
   --logger_debug("satan!")
  end
  if response!=nil then   
    local response_topic_id=response.topic_id+0
    response.directed_to_id=sayer_unique_id
    if (response_topic_id)%10==2 and topic_id_int<2200 then
      cl_ai_ask_player_to_leave(this,topic_id_string)
    end
    if topic_id_int==1031 then
      this.allowed_player_to_search_house=response_topic_id>1035
    end



    if topic_id_int==201 then
        --logger_debug("aaaa")
      if response_topic_id>205 then
        local first_name,second_name,third_name
        foreach(gl_ai_names,function(name) 
            if name ~= this.dialog_name then
              if first_name==nil then 
                first_name=name 
              elseif second_name==nil then 
                second_name=name
              elseif third_name==nil then 
                third_name=name
              end
            end
          end)
        if response_topic_id>207 and #gl_ai_names>3 then
          response.msg=first_name..", "..second_name.." and "..third_name
        if(not table_contains(gl_player.known_names,third_name)) add(gl_player.known_names,third_name)
        else
          response.msg="there is "..first_name.." and "..second_name
        end
        if(not table_contains(gl_player.known_names,first_name)) add(gl_player.known_names,first_name) 
        if(not table_contains(gl_player.known_names,second_name)) add(gl_player.known_names,second_name) 
      end
    elseif topic_id_int==1041 then
      local recluse_condition=this.dialog_name~=gl_recluse_name
      if response_topic_id>1045 then  
          response.msg= recluse_condition and gl_recluse_name.." moved in about month ago?" or "nope, nobody new here" 
        if response_topic_id<1048 then     
          if this.is_maggus then
            response.msg=gl_recluse_name.." a month ago and recently "..first_name or first_name.." moved here a week ago"
            if(not table_contains(gl_player.known_names,first_name)) add(gl_player.known_names,first_name) 
          else
            response.msg=recluse_condition and gl_recluse_name.." a month ago and recently "..gl_maggus_fake_name or gl_maggus_fake_name.." moved here a week ago"
            if(not table_contains(gl_player.known_names,gl_maggus_fake_name)) add(gl_player.known_names,gl_maggus_fake_name)
          end
        end
        if(not table_contains(gl_player.known_names,gl_recluse_name)) add(gl_player.known_names,gl_recluse_name) 
      end
    elseif topic_id_int==1051 and response_topic_id>1054 then
      local name=sub(msg.msg,11,#msg.msg-1) --"..." adds space when inputed into pico code
      --logger_debug("where is:"..name..";")
      if this.dialog_name==name then
        response.msg="i am here..."
      --logger_debug("i am me")
      elseif name == "maggus" then
        response.msg="i don't know anyone by that name"
      else
        foreach(gl_ais,function(ai)
          if ai.dialog_name==name then
            if(ai.house_desc~=nil) response.msg=ai.house_desc
            if(ai.house_desc==nil) response.msg="hm... i don't know"   
          end
          end)    
      end
    end
  end
  this.relation_table[sayer_unique_id]=relation_value
  this.conversation_tables[sayer_unique_id]=conversation_table
  return response
end

function cl_ai_get_topic_from_response_table(this,min,max,incr,topic_id,conversation_table)
  for i=min,max,incr do
    if (topic_id==210) i=210
    return cl_ai_copy_from_response_table(this,i)
  end   
  return nil
end
function cl_ai_draw(this,time_of_day_delta)
    if(this.is_in_house) return
    local ly=0
    local below=mget(pixel_tile_conv(this.x+this.hitbox_x),pixel_tile_conv(this.y+this.hitbox_y)+1)
    this.local_x=this.x-gl_camera_x
    this.local_y=this.y-gl_camera_y

    if(table_contains({203,201,96,99},below)) ly+=1

    spr(this.spr,this.local_x,this.local_y+ly,1,1,this.flip_x,this.flip_y)
end
function cl_ai_kill(this)
  del(gl_ais,this)
  cl_actor_kill(this)
end
function cl_ai_say_later(this,msg)
  if(msg.timestamp==nil) msg.timestamp=gl_app_time_timeelapsed
  add(this.say_message_queue,msg)
end
function cl_ai_generate_door_obj(this,tile_x,tile_y)
  this.door_obj_x=tile_x
  this.door_obj_y=tile_y
  --logger_debug("house:"..tile_x..";"..tile_y)
end
function cl_ai_generate_house_desc(this,pixel_x,pixel_y)
  if(pixel_x==136 and pixel_y==232) this.house_desc="around the farm house"
  if(pixel_x==656 and pixel_y==184) this.house_desc="house between slopes"
  if(pixel_x==736 and pixel_y==32) this.house_desc="house on the highest slope"
  if(pixel_x==904 and pixel_y==232) this.house_desc="probably sitting, as always"
  --logger_debug(pixel_x..";"..pixel_y..";h_desc:"..(this.house_desc==nil and "none" or this.house_desc))
end
function cl_ai_terrorized(this)
  this.is_terrorized=true
  return cl_ai_copy_from_response_table(this,"-123") 
end
function cl_ai_ask_player_to_leave(this,topic_id_string)
  local response_id=1022
  this.ask_player_to_leave+=1
  if topic_id_string~=nil and topic_id_string=="-666" and this.ask_player_to_leave<5 then 
    response_id=-667 
  else
    response_id=2300+min(this.ask_player_to_leave,9)
  end
  if(this.ask_player_to_leave==4) gl_game_results_angered_npcs+=1
  return cl_ai_copy_from_response_table(this,response_id)
end

function cl_mercenary_ai_init(this,x,y)
    cl_ai_init(this,x,y)

    local intro1="2.5;wake up;f1;3;hey, wake up;;3.5;get up, quickly!;;3;bridge is destroyed;f1;4;could be done by our target...;f2;3;now, remember;f1;3;we are searching for maggus,;;3;which was royal mage before;;4;may be dangerous...;;3;but is worth more alive;;3;i will guard the exit;;3;till you get back with mage;f2;4;we have two days for this...;f1;3;good luck...;;1;"

    this.dialogs=explode_internal(intro1,{"length","msg","action"})

    this.next_dialog_time=-1
    this.process=cl_mercenary_ai_process

    cinematic_turn_on()
end
function cl_mercenary_ai_process(this)
  if(this.killed) return
  if(this.x<0 or this.x>1024) this.kill(this)
  local seconds=flr(gl_app_time_timeelapsed)
  local t=gl_app_time_timeelapsed
  if #this.dialogs>0 and gl_cinematic_turned_on then
    if t>=this.next_dialog_time then
      local obj=this.dialogs[1]
      if(obj==nil) return
      this.next_dialog_time=t+(obj.length or 1)
      this.say(this,obj)
      if obj.action!=nil then
        this.flip_x=obj.action=="f1"
      end
      del(this.dialogs,obj)
    end
  else
    this.left=true
    cinematic_turn_off(false)
  end
end


function cl_farmer_ai_init(this,x,y)
  cl_ai_init(this,x,y)
  local doors_x,doors_y=cl_actor_find_doors(this)

  this.own_a_house=true
  cl_ai_generate_door_obj(this,doors_x,doors_y)
  cl_ai_generate_house_desc(this,doors_x,doors_y)

  this.farming_process=0
  this.reverse_farming=false
  
  this.process=cl_farmer_ai_process  
  this.is_ai_terrorized=cl_farmer_will_be_terrorized
  gl_farmer_spawned=true
  --this.maxrun=min(0.4,this.maxrun)
end
function cl_farmer_ai_process(this)
  cl_ai_process(this)
  if(not this.given_command) cl_farmer_ai_behaviour(this,false)
end
function cl_farmer_ai_behaviour(this,is_maggus)
  --logger_debug(this.farming_process)
  if this.is_in_house or this.time_to_sleep then 
    this.down=false
    --logger_debug("slip time")
    return
  end
  local proccess=this.farming_process
  local delta_start_x=164-this.x
  local delta_end_x=192-this.x
  if proccess==0 then
      this.left=delta_start_x<0
      this.right=delta_start_x>0 
      proccess=pixel_tile_conv(delta_start_x)==0 and 1 or 0
  else
    --for i=160,192 do
      local farming_value=is_maggus and 0.04 or 0.01
      proccess+=(this.reverse_farming and -farming_value or farming_value)
      proccess=max(min(100,proccess),0)   
      this.reverse_farming= this.reverse_farming and proccess>0 or proccess==100  
      local delta_farming_x=164+(proccess*0.30)-this.x 
      --logger_debug(delta_farming_x)
      this.left=delta_farming_x<-3
      this.right=delta_farming_x>3
      local abs_delta=abs(delta_farming_x)
      local p=abs_delta%1.0
      this.down=(p>0.1 and p<0.3) or (p>0.45 and p<0.55) or (p>8.5 and p<0.98)

  end
  this.farming_process=proccess
  if not is_maggus and not this.is_terrorized then
    if gl_player.x>160 and gl_player.x<192 and pixel_tile_conv(gl_player.y)==29 and this.yelled_at_player_cooldown<0 then
      local response=cl_ai_ask_player_to_leave(this)
      response.msg=maybe() and "my crops!! use ladder!" or "get out and use ladder!"
      this.say(this,response)
      this.yelled_at_player_cooldown=240
    else
      this.yelled_at_player_cooldown=max(-1,this.yelled_at_player_cooldown-1)
    end
  end
end
function cl_farmer_will_be_terrorized(topic_id_int)
  return topic_id_int==2203 or topic_id_int==2205
end

function cl_royal_ai_init(this,x,y)
  cl_ai_init(this,x,y)
  local doors_x,doors_y=cl_actor_find_doors(this)

  this.own_a_house=true
  cl_ai_generate_door_obj(this,doors_x,doors_y)
  cl_ai_generate_house_desc(this,doors_x,doors_y)

  this.process=cl_royal_ai_process
  this.is_ai_terrorized=cl_royal_will_be_terrorized
  gl_royal_spawned=true

  this.maxrun=0.2
end
function cl_royal_ai_process(this)
  cl_ai_process(this)
  if(not this.given_command) cl_royal_ai_behaviour(this,false)
end
function cl_royal_ai_behaviour(this,is_maggus)
  if this.is_in_house or this.time_to_sleep then 
    this.sitting=false
    this.is_sitting_up=false
    return
  end

  local delta_x=912-this.x --x of chair
  local at_chair=abs(delta_x)<1

  this.sitting_cooldown=this.sitting_cooldown and this.sitting_time!=1
  this.sitting=at_chair and not this.sitting_cooldown
  this.left= not at_chair and delta_x<0
  this.right=not at_chair and delta_x>0
  this.is_sitting_up=at_chair and this.is_sitting_up or false
  if this.sitting and this.sitting_time<(is_maggus and 600+rnd(240) or 2100) and not this.sitting_cooldown then
    this.sitting_time+=1
  else
    this.sitting=false
    this.is_sitting_up=false
    this.sitting_cooldown=true
    this.sitting_time=max(1,this.sitting_time-4)
  end
end
function cl_royal_will_be_terrorized(topic_id_int)
  return topic_id_int==2201 or topic_id_int==2202 or topic_id_int==2205
end

function cl_recluse_ai_init(this,x,y)
  cl_ai_init(this,x,y)
  local doors_x,doors_y=cl_actor_find_doors(this)
 
  this.process=cl_recluse_ai_process
  this.is_ai_terrorized=cl_recluse_will_be_terrorized

  gl_recluse_name=this.dialog_name
  if doors_x~=nil then
    this.own_a_house=true
    cl_ai_generate_door_obj(this,doors_x,doors_y)
    cl_ai_generate_house_desc(this,doors_x,doors_y)
    --logger_debug("rcls h:"..doors_x..doors_y)
  end
  this.maxrun/=2.0
end
function cl_recluse_ai_process(this)
  cl_ai_process(this)
  if(not this.given_command) cl_recluse_ai_behaviour(this,false)
end
function cl_recluse_ai_behaviour(this, is_maggus)
  if (this.is_in_house or this.time_to_sleep) return

  local right=cl_object_is_solid(this,4,1) and not cl_object_is_solid(this,4,0) and this.x<1000

  this.left=cl_object_is_solid(this,-4,1) and not cl_object_is_solid(this,-4,0) and this.x>8 and not this.right --if is already going right, allow it
  this.right=not this.left and right
end
function cl_recluse_will_be_terrorized(topic_id_int)
  return (maybe() and topic_id_int==2201) or topic_id_int==2204 or (maybe() and topic_id_int==2202)
end
function cl_maggus_init(this,x,y,recluse_x,recluse_y)
  cl_ai_init(this,x,y)

  this.is_ai_terrorized=cl_maggus_will_be_terrorized
  this.process=cl_maggus_process

  this.is_maggus=true
  gl_maggus_fake_name=this.dialog_name

  local doors_x,doors_y=cl_actor_find_doors(this)
  --if(doors_x==nil) run()
  local didnt_find_a_house=true
  if doors_x~=nil then
    if cl_maggus_consult_staying_at_house(this,doors_x,doors_y) then
      this.x=doors_x
      this.y=doors_y

      cl_ai_generate_door_obj(this,doors_x,doors_y)
      this.is_in_house=true
      --logger_debug("in da house:"..pixel_tile_conv(doors_x)..";"..pixel_tile_conv(doors_y))
      didnt_find_a_house=false
    end
  end
  if doors_x==nil or didnt_find_a_house then
      this.mimics_farmer=not gl_farmer_spawned
      this.mimics_royal=not gl_royal_spawned and not this.mimics_farmer

      if this.mimics_royal then
        this.x=tile_pixel_conv(114)
        this.y=tile_pixel_conv(29)
        
        this.moving_time=0
        --logger_debug("royal mimic")
      elseif this.mimics_farmer then
        this.x=tile_pixel_conv(19)
        this.y=tile_pixel_conv(29)
  
        this.farming_process=0
        this.reverse_farming=false
        --logger_debug("farmer mimic")
      else
        this.mimics_recluse=true
        this.x=recluse_x
        this.y=recluse_y
        this.maxrun+=0.25
        --logger_debug("recluse mimic")
      end
  end

  --logger_debug("maggus:"..pixel_tile_conv(this.x)..";"..pixel_tile_conv(this.y))
end
function cl_maggus_consult_staying_at_house(this,door_x,door_y)
  local stay=true
  foreach(gl_ais,function(ai) 
    if ai.door_obj_x!=nil then
      if ai.unique_id!=this.unique_id and ai.door_obj_x==door_x and ai.door_obj_y==door_y then
        stay=cl_ai_get_starting_relation_value_based_on_perks(ai,this.unique_id)>25
        ai.maggus_staying_in_house=stay
      end
    end
  end)
  return stay
end
function cl_maggus_init_mimic_settings(this) 
  cl_generate_default_values_for_ids_from_table(this,explode_internal("mimics_farmer;bool;mimics_royal;bool;mimics_recluse;bool;is_sitting_up;bool;sitting;bool;sitting_cooldown;bool;",gl_variable_init_table))

      this.sitting_time=1

end
function cl_maggus_process(this) 
  if this.is_in_house then 
    return
  end

  cl_ai_process(this)
  this.time_to_sleep=false
  this.is_asleep=false
  this.lying_down=false


  if(this.mimics_farmer) cl_farmer_ai_behaviour(this,true)
  if(this.mimics_royal) cl_royal_ai_behaviour(this,true)
  if(this.mimics_recluse) cl_recluse_ai_behaviour(this,true)

  gl_game_results_got_maggus=this.given_command
  --logger_debug(this.x..";"..this.y..(this.left and ";lft" or ";rght")..(up and ";up" or ";dwn")..(this.knows_is_hunted_by_player and ";hnt" or ""))
end
function cl_maggus_will_be_terrorized(topic_id_int)
  return topic_id_int==2201 or topic_id_int==2204
end
__gfx__
00006600000066000000000000000000000000000000000000006600000066000000660000006600000066000006670000066000000660000006600000066000
00006600000066000000660000006600000000000000000070006670000066000000660000006600000066000006700000066700000660000006600000066000
00666000006660000000660000006600000000000000000007666700706660700066600000666000006660000067600000667000006660000066600000666000
00666000006660000066600000666000000000000000660000766000077667007776677007766700007660000066600000676000006777000067600000676000
00666000006660000066600000666000000066000066660000666000006660000066600070666070076667000066600000666000006660000066700000667000
00666000006660000066600000666000006666000066600000666000006660000066600000666000706660700066600000666000006660000066670000667000
00666000006660000066600000666000006660000066600000666000006660000066600000666000006660000066600000666000006660000066600000666000
00666000006660000066600000666000006660000066600000666000006660000066600000666000006660000066600000000000000000000000000000666000
00066000000000000000000000000000000000000000000000000000000000000000660000006600000000000000000000066000000660000006600000066000
00066000000000000000000000000000000000000000000000000000000066000000660000006600000066000000660007066000000660000006607000066000
00666000000000000000000000000000000000000000000000000000000066000066600000666000000066000000660007666600076666700066667007666670
00676000000000000000000000000000000000000000000000006600006660000067600000676000006660000066600007666670076666700766667007666670
00676000000000000000000000660000000660000000660000666600006760000067600000676000006760000067600000666670006666000766660000666600
00667000660000000066000000660000000660000066660000676000006760000067600000676000006760000067600000666600006666000066660000666600
00666000666666600667666000676600006766000067600000676000006760000066600000666000006760000067600000666600006666000066660000666600
00666000666666600666776000077600006766000067600000676000006660000066600000666000006660000066600000666600000000000066660000000000
00006600000066000006600000066000006600000006600000006600000660000066000000006600000066000000660000006600000066000000660000006600
00006600000066000006600000066000006600000006600000006600000660000066000000006600000066000000660000006600000066000000660000006600
00666000006660000066600000666600006666000066660000666600006666000066660000666000006660000066600000666000006660000066600000666000
00666000006660000066600000666600006666000066660000666600006666000066660000676000007760000067600000676000006760000067600000677700
00666000006660000066600000666600006666000066660000666600006666000066660000676000007660000067600000677000006677000066770000666000
00666000006660000066600000666600006666000066660000666600006666000066660000676000006660000066600000666000006660000066600000666000
00666000006660000066600000666600006666000066660000666600006666000066660000666000006660000066600000666000006660000066600000666000
00666000000000000000000000000000000000000000000000000000000000000000000000666000006660000066600000666000006660000066600000666000
00006600000066000000660000006600000066000000660000006600000066000000660000006600000000007767777776777777ccc00cc00ccc00cc90000009
000066000000660000006600000066000000660000006600000066000000660000006600000066000000000077766677677d7767c0c0c00c0ccc00cc99000099
0066600000666000006660000066600000666000006660000066600000666000006660000066600006000000d7d77d677dd77dddc0c000c0ccccc0cc90900909
0067770000677700006777000067770000677700006760000067600000676000006760000067600076777770d6dd77677d77ddddccc00c00cc0cc0cc90099009
0066600000666000006660000066600000666000006677000066770000677000006760000067600006000000d6ddd777777dddddc0c0c00ccc0cc0cc90099909
0066600000666000006660000066600000666000006660000066600000666000006660000067600000000000d766667777ddddddc0c00cc0ccccc0cc90090009
0066600000666000006660000066600000666000006660000066600000666000006660000066600000000000d6dddd677dddddddc0c00000cc0cc0cc90009009
0066600000666000006660000066600000666000006660000066600000666000006660000066600000000000d6dddd677dddddddc0c000c0cc0cc0cc90999009
dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd77dddddd7777777777777777d7777ddd7777777777777777d6dddd6766666666
dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd77dddddd7666666766666666766677777dddddddddddddd7ddd6dd77666ddd66
dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd77dddddd7666666766666666d66666677dddddddddddddd7dddddddddddddddd
dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd77dddddd76666667666666667d66666d7dddddddddddddd7d6dddddddddddddd
7777dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd77dddddd76666667666666667666666d7dddddddddddddd7d6dddd6ddddddddd
777777777777ddddddd777777777ddd7ddd77ddddddddddddddddddd7777ddd7d77ddddd7666666766666666766666677dddddddddddddd7dddddddddddddddd
ddd777777777777777777777777777777777777d777dddddddddddd777777777d77ddddd7666666766666666777666777dddddddddddddd7d6dddd6ddddddddd
dd77ddddddd777777777ddddddd7777d7777d777777ddddddddddd77d777777dd77ddddd777777777777777777777ddd7dddddddddddddd7ddddddd7dddddddd
dd77ddddddd77ddddd77ddddddd77ddddd77d77dddddddddddddddddd77dd77777dddddd766666677666666777dd77ddddddddd700000000dddddddd6666dddd
dd7dddddddd77ddddd7dddddddd77ddddd7dd777dddddddddddddddd777dd7dd77dddddd7666666776666667d7777ddddddddd7600000000dddddddd6ddddddd
dd7ddddddddd7ddddd7ddddddddd7ddddd7dd777ddddddddddddddd7777dd7dd77dddddd7666666776666667dd676dddddddddd700000000dddddddddddddddd
dd77ddddddd77ddddd7ddddddddd7ddddd7dd7777dddddddddddddd7d77dd77d77dddddd7666666776666667ddddddddddddddd700000000dd77dddddddddddd
dd77777777777ddddd7ddddddddd7ddddd7dd77d7dddddddddddd777d77ddd7d77dddddd7666666776606667ddddddddddddddd70000000077dddddddddddddd
d77ddd77dddd7dddd77ddddddddd7dddd77dd77d777dddddddddd7ddd77ddd7dd77ddddd7666666776000667dddddddddddddd7600000000d667dddddddddddd
d7ddddddddd77dddd7ddddddddd77dddd7ddd77dd77dd777ddd777ddd77ddd7dd77ddddd76666667760000077dddddddddddd7d70000000066667d7ddddddd6d
d7ddddddddd7ddddd7ddddddddd7ddddd7ddd77dd7777777ddd7ddddd77ddd7dd77ddddd76666667700000007ddddddd767776770000000076776667ddd66d66
7777dddd7777777777777777ddd77777ddd766dd77777777777777777777777777777777dddddddddddddddd7ddddddd76ddddddd6dddd67dddddddddddddddd
77777777777777777777777777777777ddd67ddd77777777777777777777777777777777dddddddddddddddd67dddddd676dddddd7666677dddddddddddddddd
dddd7777dddddddddddddddd777ddddddd677ddddd7777dd7dddddddddddddddddddddd7dddddddddddddddd7ddddddd777dddddd6dddd67dddddddddddddddd
ddddddddddddddddddddddddddddddddddd77dddddd77ddd7dddddddddddddddddddddd7dddddddddddddddd7dddddddd676dddddddddd67dddddddddddddddd
ddddddddddddddddddddddddddddddddddd766ddddd766dd7dddddddddddddddddddddd7dddddddddddddddd7ddddddd7677677dddddddd7dddddddddddddddd
ddddddddddddddddddddddddddddddddddd67dddddd67ddd7dddddddddddddddddddddd7dddddddddddddddd67dddddd67767777d76dddd7dddddddddddddddd
dddddddddddddddddddddddddddddddddd667ddddd667ddd7dddddddddddddddddddddd777ddddddddddddd777ddd7dd777d7767dddddd67d7dddddddddddddd
ddddddddddddddddddddddddddddddddddd77dddddd77ddd7dddddddddddddddddddddd777d77dddd7ddddd77677767776777677ddddddd77677d6dd76d77677
0dddddd06666666066600000dddd6666ddd766ddddd766dd7d777dddddddddd6ddddddd7dddddddddddddddd00000000ddddddddd6dddd6d0000000000000000
0d0000d06666666666600000dddd7676ddd67dddddd67ddd7d7dddddddddddddddddddd7dddddddddddddddd00000000ddddddddd766667d0000000000000000
0dddddd06666006666000000ddd67776dd677ddddd677ddd7d7ddddd6ddd6666ddddddd7dddddddddddddddd00000000ddddddddd6dddd6d0000000000000000
0dddddd0dd0001d000000000ddd67666ddd77dddddd77ddd7d77dddd6d666666ddddddd7dddddddddddddddd00000000ddddddddd6dddd6d0000000000000000
01111110dd001d000000000066666666ddd766ddddd766dd7dd7dddd66666666ddddddd77ddddddddddddddd00000000ddddddddd6dddd6d0000000000000000
0dddddd0dd01d0000000000066666666ddd67dddddd67d777dd7ddddd6666666ddddddd7ddddd7dddddddddd00000000ddddddddd766667d0000000000000000
0dd00dd0dd1d00000000000076666666dd6666dd776677777dd7dddd66666666ddddddd7777d777dddddd77d07000700d7ddd7ddd6ddd76d0700000000000000
0dd00dd0ddd000000000000076666666ddd76ddd777777777dd7dddd66666666ddddddd77777777777dd777d7677767776777677767776677677060076077677
60d000d000ddddd000d000d000000d06000000000000000000000000000000000000000000000000000000000666666666660666666666666666666666666000
7ddddddddddd0ddddddddddd00d00dd700000000000000000000000000000000000000000000000000000000666666666666666666666d6dddd6666666666606
60d000d00d0000d000d000d00dd0dd0600000000000000000000000000000000000000070000000700000000dddddddddddddddddddddddddddddddddddddddd
7ddddddd000000dddddddddd0dddddd707700000000000000000000070000000000700077070070700000000dddddddddddddddddddddddddddddddddddddddd
60d000d0000000d000d000d00d000d0670070000077000000000000070077000007777070777707000000000dddddddddddddddddddddd1111dddddddddddddd
7ddddddd7dddddddddddddddddddddd707077770777700700707077707007000707777777777777007000770dddddddddddddddddddd11111111dddddddddddd
60d070d770707070707070707d707d7677777777777777777777777777777777777077777777777777777777ddddddddddddddddddd1111111111ddddddddddd
6667676776767676767676767677767677707770000070707770777007777770070070700700707077077770ddddddddddddddddddd1111111111ddddddddddd
6666666666666666666666666666600067666676000000066666666666666666666666666666666666666666dddddddddddddddddddd111111111ddddddddddd
6666666666666666666666666666660067777776000000066666666666666666666666666666666666666666dddddddddddddddddddd111111111ddddddddddd
6666dd66dd6666dddd6666dd6666d600676666760000000d6666dd6666dd6666dd6666dd6666d6666666d666dddddddddddddddddddd111111111ddddddddddd
ddddddddddddddddddddddddddddddd0676666760000000ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd111111111ddddddddddd
dddddddddddddddddddddddddddddddd676666760000000d0dddddddddddddddddddddddddddddddddddddddddddddddddddddddddd1111111111ddddddddddd
dddddddddddddddddddddddddddddddd677777760000000d0dddddddddddddddddddddddddddddddddddddd0ddddddddddddddddddd1111111111ddddddddddd
dddddddddddddddddddddddddddddddd676666760000000d0dddddddddddddddddddddddddddddddddddddddddddddddddddddddddd1111111111ddddddddddd
dddddddddddddddddddddddddddddddd6766667600000006dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd111111111ddddddddddd
dddddddddddddddddddddddddddddddd6666666666666666ddddddddddddddddddddddddddddddddddddddddddddddd66ddddddd000000000000000000000000
dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd6666dddddd000000000000000000000000
ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd0ddddd666666ddddd000000000000000000000000
ddddddddddddddddd1111111dddddddddddddddddddddddd0dddddddddddddddddddddddddddddddddddddd0dddd66666666dddd000000000000000000000000
ddddddddddddddddd11111111ddddddddddddddddddddddd00ddddddddddddddddddddddddddddddddddddd0ddd6666666666ddd000000000000000000000000
ddddddddddddddddd11111111ddddddddddddddddddddddd0ddd111111166dddddddddddddddddd1111111d0dd666666666666dd000000000000000000000000
ddddddddddddddddd11111111dddddddddddd1111111dddd0ddd11111116666dddddddddddddddd111111dddd66666666666666d770000000000000700000077
ddddddddddddddddd11111111dddddddddddd1111111dddddddd11111116666dddddddddddddddd111111ddd6666666666666666770770000700000777077777
ddddddddddddddddd11111111dddddddddddd1111111dddddddd11111116666ddddddddddddddd1111111ddd6666666666666666077000000000000000000000
1dddddddddddddddd11111111dddddddddddd1111111dddddddd11111116666ddddddddddddddd1111111ddd6666666dd6666666077000000ddd00000dd00000
ddddddddddddddddd11111111dddddddddddd1111111dddddddd11111116666ddddddddddddddd11111111dd666666dddd66666607700d000ddd00000dd00000
ddddddddddddddddd11111111dddddddddddd1111111dddddddd11111116666ddddddddddddddd1111111ddd66666dddddd666667700dd00770dd0000dd00000
ddddddddddddddddd11111111dddddddddddd1111111ddddddddddddddddd66dddddddddddddddd111111ddd6666dddddddd66667700d000770dd0707dd00000
ddddddddddddddddd11111111dddddddddddd1d111d1ddddddddddddddddddddddddddddddddddd1111111dd666dddddddddd666770dd770770dd7707dd00070
1dddddddddddddddd11111111dddddddddddd1ddddd1dddd0dddddddddddddddddddddddddddddd111111ddd66dddddddddddd66770dd777770dd77d77d77777
1dddddddddddddddd11111111dddddddddddd1ddddd1ddddddddddddddddddddddddddddddddddd111111ddd6dddddddddddddd6770dd777770d777d77077777
00000000000000000000000000000000000000000000000006000060d6dddd6d77777777777700007777777700077777d6dddd67d6dddd6d6766667606000060
00000000000070000000000000000000000000000000000007666670d766667d77777777777777777777777777777777d7666677d766667d6777666607666670
07000000000707000000000000700000000000000000000006000060d6dddd6dd6dddd6d000077770000000077700000d6dddd67dddddd6d6666666600000060
70700000770700007070070700777077000000000000000006000060d6dddd6dd6dddd6d000000000000000000000000d6dddd67dddddddd6666667606000000
00070000007000000707707007070700000000000000000006000060d6dddd6dd6dddd6d000000000000000000000000d6dddd67dddddd6d6666666600000000
0707007007070070070707700707077000000000777700070766667077766777d766667d000000000000000000000000d7666677dddddd7d6776676600000000
7777777777777777777777777777777700000007777777770600006077777777d6dddd6d000000000000000000000000d6dddd67dddddddd6766666600000000
77707770000070700700707077077770000000770777777006000060d777776dd6dddd6d000000000000000000000000d6dddd67d6dddddd6666667600000060
000000000000000000000000000770000000000007700777d6dddd6dddddddd77ddddddd0007660077777777666666660000000d0000000660000000d0000000
000000000000700000077700007007000000000077700700d766667ddddddd7667dddddd000670007777777766666666000000dd0000006666000000dd000000
070077000007070077770777007000000000000777700700d6dddd6dddddddd77ddddddd0067700000777700d666666600000ddd0000066666600000ddd00000
707770077707000070700707777770700000000707700770d6dddd6dddddddd77ddddddd0007700000077000dd6666660000dddd0000666666660000dddd0000
000700770070777707077070070707070000077707700070d6dddd6dddddddd77ddddddd0007660000076600d6666666000ddddd0006666666666000ddddd000
070700700707707077070770070707770000070007700070d766667ddddddd7667dddddd0006700000067000d666666600dddddd0066666666666600dddddd00
777777777777777777777777777777770007770007700070d6dddd6dddddddd77ddddddd0066700000667000d66666660ddddddd0666666666666660ddddddd0
777077700777777007007070770777700007000007700070d6dddd6dddddddd77ddddddd000770000007700066666666dddddddd6666666666666666dddddddd
000000000000000000000000000000000000000000000000676666760000000770000000000766000007660066666666dddddddd6666666666666666dddddddd
000000000000000000000000000000000000000000000000677777760000007667000000000670000006700066666666ddddddd066666660066666660ddddddd
000000000000000000000000000000000000000000000000676666760000000770000000006770000067700066666666dddddd00666666000066666600dddddd
0000000000000000000000000000000000000000000000006766667600000007700000000007700000077000d6666666ddddd0006666600000066666000ddddd
7777000000000000000000000000000000000000000000006766667600000007700000000007660000076600dd666666dddd000066660000000066660000dddd
7777777777770000000777777777000700077000000000006777777600000076670000000006700000067077ddddd666ddd00000666000000000066600000ddd
0007777777777777777777777777777777777770777000006766667600000007700000000066660077667777dddddd66dd0000006600000000000066000000dd
0077000000077777777700000007777077770777777000006766667600000007700000000007600077777777ddddddd6d000000060000000000000060000000d
007700000007700000770000000770000077077000000000770000000770000000770007000000000000000011111111dddddddd6666666677777777eeeeeeee
007000000007700000700000000770000070077700000000770000000770000007700076000000000700000011111111dddddddd6666666677777777eeeeeeee
007000000000700000700000000070000070077700000000770000000770000007700007000000000700000011111111dddddddd6666666677777777eeeeeeee
007700000007700000700000000070000070077770000000770000007700000077000007000000000700000011111111dddddddd6666666677777777eeeeeeee
007777777777700000700000000070000070077070000000770000007700000077000007000000007700d00711111111dddddddd6666666677777777eeeeeeee
07700077000070000770000000007000077007707770000007700000770000007700007600000000d77ddd7711111111dddddddd6666666677777777eeeeeeee
07000000000770000700000000077000070007700770077707700000770000007700000770000000d77dd77d11111111dddddddd6666666677777777eeeeeeee
07000000000700000700000000070000070007700777777707700000770000007700000777000000d77d77dd11111111dddddddd6666666677777777eeeeeeee
__label__
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999909999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999919999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999199999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111199999999911111
99999999999999999999999999999999999999999999999999999999999999999999999999999111111111111111111111111111111111111199999999911111
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999911111
99999999999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111111111
99999990999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111111111
99999999999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111111111
11111111999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111199999
11111111999999999999999999999999999999999999999999999999999991111111111111111111111111111111111111111111111111111111111111199999
11111111991111111111111111111111111111111111111111111111111111111111111111111111111111111111119999999999999999999999999999999999
11111111991111111111111111111111111111111111111111111111111111111111111111111111111111111111119999999999999999999999999999999999
11111111991111111111111111111111111111111111111111111111111111111111111111111111111111111111119999999999999999999999999999999999
11111111991111111111111111111111111111111111111111110011111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111111111111111111111110011111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111119999999999999999999911111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111119999999999999999999911111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111119999999999999999999911111111111111111111111111111111111111119999999999999999999999999999999999
11111111111111111111111111111111119999999999999999999999999999999999999999999999999999999999999999999999991199999999999999999999
11111111111111111111111111111111119999999999999999999999999999999999999999999999999999999999999999999999991199999999999999999999
11111111111111119999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999919999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999909999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
aaaaaaaaaaaaaaaaaaaaaaaaaaaa00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
99999999999999999999999999990099999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999909999
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaa000000000aaaaaaa00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaa00000000000000000000000a000aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaa0000aaaaaaa000000000a000000aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaa00aaaaaa00aaaaa00a00aaaaaaaa11aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaa00aaaaaa00aaaaa0aa000aaaaaaa11aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaa0aaaaaaa0aaaaa0aa000aaaaa111aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaa0aaaaaaa0aaaaa0aa0000aaaa111aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaa0aaaaaaa0aaaaa0aa0000aaaa111aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaaa0aaaaaaa0aaaa00a000a000aa001aaaaaaaaaaaaaaaaaaaaaaaaaaa1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
aaaaaaaaaaaa00aaaaaa00aaaa0aa000aa00a00000aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
0aaaaaaaaaaa0aaaaaaa0aaaa000000000000000000aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
0111111111111111111111111100010001010111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaa0aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1a
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa1aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaa0aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
1111111111111111111111111111111111111111100aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
11111111111111111111111111111111111111111a00aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

__gff__
0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000272300000020202020202020202043434320202420202020202020202020434320200020202323232320232020202020202024202020230020202020432020208020248080
202020208080808080808023232323232323232324202323232323202020302020202020232320202020204b4b80808020203020203020202020304b4b80808080808080808024242783838324242724808080808080242020808343204b4b208080808080802720208080432043432080808080808080802020202020438300
__map__
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfdfdfdfdfdfdfd
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcbcfdbbbcfdfd
fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffcfcfcfcfcdbfd
ffffffffffe2e1e4e5ffffffffffffffffffffffffffffffffffffffc4c5e1e2e3e4e2e1e1e2e1e2e3e1e1e2e2e2e3e1e1e2e1e2e3e1e1e2e1e2e3e1e1e2e1e2e3e1e1e2e1e2e3e1e1e2e1e2e3e3e1e1e2e1e2e3e1e1e2c5ff8b8c8d8e8fffc5e1e2e1e1e2e1e2e1e1e2e1e2e1e1e2e1e2e1e1e2e1e2e3e2e3e1e1fcfcfcebfd
7efffffffff3f3f4f5f9ffffffffffffffffffffffffffffffffffffd4d5f1f2f3f4f5f3f2f2f3f2f3f3f0f1f3f2f0f1f3f2f2f3fff2f3ffd4d5f1f2f3f2f2f1f3f4f5f3f2f2f3f2f3f3f3f2f2f3f2f3f3f0f1f3f2f0f1d53f9b9c9d9e9f3df4f3f2f2f3f2f3f3f0f1f3f2f0f3f2f2f3f2f3f3f0f1f3f2f0f1d4d5f1797377fd
fd3c3bfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffdfdfdfdfde6fdfdfdfdc9cac9cbcac9c9cadac9c9cacac9c9c9cacbc9cacbfdfdfdfdfdfde6fdfdcefdc9cac9cbcac9cbc9dac9cbcac9cbcacac9c9cacafde6fdfdfdfde6fdfdc9cac9cbcac9cbcacac9c9cac9dac9cbcac9cbcacac9c9cacae6fdfdfdfdfdfd
fdd8ccfdfdfdfdfdfdf7fffffffffffffffffffffffffffffffdfdfdfcfcd6fcfcfcfcffffffffffffffffd9fffffffffffffffffffffffffffcfcfcfc65d665fcfcfcfcffffffffffffffd9fffffffffffffffffffffffcd6fcfcfcfcd6fcfcfcffffffffffffffffffffffffd9fffffffffffffffffffffc94fdfdfdfdfdfd
fdd8ccfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffffffffcfcd6fcfcfcfcffffffffffffffffd9fffffffffffffffffffffffffffffcfcfc64d664fcfcfcfcffffffffffffffd9fffffffffffffffffffffffccdfcfcfcfcd6fcfcfcffffffffffffffffffffffffd9fffffffffffffffffffffc94fdfdfdfdfdfd
fdd8ccfdfdfdfdfdfdf7fffffffffffffffffffffffffffffffffffffcfcd6fcfcffffffffffffffffffffd9fffffffffffffffffffffffffffffffcfc64d664fcfcffffffffffffffffffd9fffffffffffffffffffffffccdfcfcfcfcd6fcfcfcffffffffffffffffffffffffd9ffffffffffffffffffffffc6fdfdfdfdfdfd
fdd8ccfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffffffffcfcd6fcfcffffffffffffffffffffeae1e4e2e1e2e3e1e1e2e2e2e3e1e1e2434275c7754442e3e1e1e2e2e2e3e1e1eae1e2e3e1e1e2e2e3e1e1e241e2424445fcd64647fcffffffffffffffffffffffffd9ffffffffffffffffffffffd6fdfdfdfdfdfd
fdd8ccfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffffffffffcd6fcfcffffffffffffffffffffeaf1f4f3f2f2f3f2f3f3f0f1f3f2f0f15352fcd6525353f0f1f3f2f0f1f3f2f2d9f2f3f3f0f1f3f2f0f1f3ff53f4fc5455fcd65657fcfffffffffffffffffffff6f5e9ffffffffffffffffffffffd6fdfdfdfdfdfd
fdd8ccfdfdfdbbfcdff7fffffffffffffffffffffffffffffffffffffffcd6fcfcffffffffffffff3fffe7fefefec9cac9cbcac9cbcbcadac9caca606262c8626060cacacbc9c9cac9c9c9dacac9cbcbcac9c9cacac9c960e6fdfdfdfde6fdfd58fffffffffffffffffffffdfdfdfdffffffffffffffffffffd6fcfdfdfdfdfd
fdd84e4f5ffcfcfcecf6fffffffffffffffffffffffffffffffffffffcfcd6fcfcfffffffffffffdfdfde6fdfdfdffffffffffffffffffd9fffffffcfcfcd6fcfccdfcffffffffffffffffd9ffffffffffffffffffffffefcdfcfcfcfcd6fcfcfcfffffffffffffffffffffcfdfdfdffffffffffffffffffffd6fcfdfdfdfdfd
fd4bcc77fd79fcfcfdf6fffffffffffffffffffffffffffffffffffffcfcd6fcfcfffffffffffffdfdfd94fdfdfdffffffffffffffffffd9fffffcfcfcfccdfcfcd6fcffffffffffffffffd9ffffffffffffffffffffffffcdfcfcfcfcd6fcfcfcfffffffffffffffffffffcfcfdfdfdfcffffffffffffffffd6fcfcfcfdfdfd
fd5bccfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffffffcfcfcd6fcfffffffffffffffcfcfcd6fcfcfcffffffffffffffffffd9fffffcfcfcfccdfcfcd6fcffffffffffffffffd9fffffffffffffffffffffffffffcfcfcfcd6fcfcfdfdfffffffffffffffffffcfcfcfcfcfcfcffffffffffffffc6fcfcfc4c4dfd
fdd8ccfdfdfdfdfdfdf7fffffffffffffffffffffffffffffffffffcfcfcd6fcfffffffffffffffffcfcd6fcfcfcffffffffffffffffffd9fffffcfcfcfcfcfcfccdfcffffffffffffffffd9fffffffffffffffffffffffffffcfcfcfcd6fcfcfdfcfffffffffffffffffffffcfcfcfcfdfdffffffffffffffc6fcfcfc6668fd
fdd8ccfdfdfdfdfdfdf6fffffffffffffffffffffffffffffffffcfcfcfcd6fcfcfffffffffffffffcfcd6fcfcfcffffffffffffffffffd9fffffffcfcfcfcfcfcfcfcffffffffffffffffd9fffffffffffffffffffffffccdfcfcfcfcd6fcfcfcfcfcfffffffffffffffffffcfcfcfcfcfcfdffffffffffffd66efc7c4c4dfd
fdd8ccfdfdfdfdfdfdf7ffffffffffffffffffffffffffffffff44454243c74342e1e2e2e3e1e1e24142c7414242e2e3e1e1e2e1e2e1e2eae1e2e14243424341414242e2e3e4e5ffffffffd9c4c5e1e2e3e4e2e1e1424142c741424141c74242424342e3e1e1e2e2e2e3e4e5fcfcfcfcfcfcfcfcfcffffffffccfdfdfd7678fd
fdd8ccfdfdfdfdfdfdf6ffffffffffffffffffffffff3fffffff54555252d652f2f2f3f2f0f1f3525251d6505153f2f0f1f3f2f2f3f2f2d9f2f3f3505153525253cdd4d5f1f4f5ffffffffe9d4d5f1f2f3f4f5f3f2525352d653525253d65350515352f0f1f3ffd4d5f1f4f56efcfcfcfcfcfcfcfcfcff3fffccfdfdfd4949fd
fdd8ccfdfdfdfdfdfdf6fffffffffffffdfdfde6fdfdfdfdfdfdfdfd6062c8cac9cacac9cacada606062c8626260c9cacac9c9c9cac9c9dacac9c9616160c86060c863fdfdfdfdfde6fdfdfdfdfdfdfdfdfdfdc961606161c860616061c8606161606063cac9c9cbfdfdfdfde6fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8d7fd
fdd8ccfdfdfdfdbbfcf7ffffffffffffeefdfd94fdfdfdfdfdbbfcfcfcfccdffffffffffffffd9fcfcfcd6fcfcfcffffffffffffffffffd9fcfcfcfcfcfcd6fcfccdfcfdfdfdfdfd94fdfdfdfdfdfdfdbbecfffffcfcfcfcd6fcfcfcfcd6fcfcfcfcfcfcfffffffffffffdfd94fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8d7fd
fdd8ccfdfdfdfdfcfff6fffffffffffffffdfd94fdfdfdfcfcfcfcfcfcfccdffffffffffffffd9fcfcfcd6fcfcfcffffffffffffffffffd9fcfcfcfcfcfcd6fcfcfcfcfcfcfdfdfd94fdfdfdfdfdfdbbecffffff5855fcfccdfcfcfcfcd6fcfcfcfcfcfcfcdfffffffffdcfcd65959fcfcfcfcfcfcfc65696afcfc65fdd8d7fd
fdd8ccfdfdfdfdfcfff6fffffffffffffffffcd6fcfcfcfcfcfcfcfcfcfcffffffffffffffffd9fcfcfcd6fcfcfcdfffffffffffffffffd9effcfcfcfcfcd6fcfcfcfcfcfcfdfdfd94fdfdfdfdfdbbec968b8ffffdfd48fcd6fcfcfed8d6fc7a7cfc7cfcfc7afcffff7e697c7d5959797c6a7c796a7c754949fcfc7459d8d7fd
fdd8ccfdfdfdbbfcffffffffffffffffffffffd6fcfcfcfcfcfcfcfcfcfcffffffffffffffffd9fcfcfcd6fcfcfcfcdfffffffffffffffd9ffeffcfcfcfccdfdfdfdfcfcfcfcfcfcd6fcfcfcfcfc3fffb09dba3dfdfd58fcd6fcd7fdfdfdfdfdfdfdfdfdfdfdd8fce7fdfdfd94fdfdfdfdfdfdfdfdfdfdfdfdfdfc7559d8d7fd
fdd8ccfdfdfdfcffffffffffffffffffffffffc6fffffcfcfcfcfc6efc7acdffffff7e7fffffd9fcfcfcd6fcfcfcfcfcffffffffffffffd9fffffcfcfcfffffdfdfcfcfcfcfcfcfcd6fcfcfdfdfdfdfdfdfdfdfdfdfd58fccdfcd7fdfdfdfdfdfdfdfdfdfdfdd8fcdfffffffc6fffcfcfcfcfcfcbcfdfdfdfdfdfdfdfdd8d7fd
fdd8ccfdfdbbfcffffffffffffffffffffffffc6fffffffcfcfcd7fdfdfdfdfdfdfdfdfde87be96ffcfcd6fc7c6ffcfc7eff7fffff7b7fe9ffffeffcfcfffffffcfcfcfcfcfcfcfce6fdfdfdfdfdfdfdfdfdfdfdfdfdf6fcfc6e5cfdfdfdfdfdfdfdfdfdfdfdfdfdd8ffffffc6ffffffffffeffcfcfcfcbcfdfdfdfdfdd8d7fd
fdd8ccfdfcfcffffffffffffffffffffffffffc6fffffffffcfcd7fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd86f6ecdfcfcfcfcfcfcfcfcfcd6fcfcfcfcfcfcfcfcfcfffffffffffc6dfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8fcffffc6ffffffffffffffeffcfcfcfc65bcfdfdd8d7fd
fdd8ccfdfcffffffffffffffffff9590919293c6fffffffffcfc5cfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8fcfcfcfcfcfcfcfcfcd6fcfcfcfcfcfcfcfcfcfffffffffcfcccfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8fcfcffc6969798999a71ffffeffcfcfc74fcbcfdd8d7fd
fdd86d5affffffffffffffffffa4a5a0a1a2a3c6ffffffffef5cfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd87c6efcfcfcfcd6fcfcfcfcfcfcfcfcecfffffffffcfcccfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd8fcfcffc6a6a7a8a9aafffffffffffffc74fcfc59d8d7fd
fd6c5e7effffffffffffffffffb8b1b0b1b2b3d68082818283fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd87cfcfcd6fc6e7c6ffc6ffcecfffffffffffcfcd6daffffdaffffdaffffdaffffffffdafcfcfcfcc6b6b7b8b9ba703fffffffffff75fcfc59d8d7fd
fdfdfdfdbfbebffaad7baff8fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdd86e7c7deaadaeeaae3feaafadeaafffadffead7fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd
fdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdfd
__sfx__
0152002009050110500e05015050150401105009050110500e05015050150401a0500a050130500e05016050130500e0500a050150500a050130500e0501c05009050110500e0501505015040110500905011050
015200200e05015050150401a0500a050130500e05016050130500e0500a050150500a050130500e0501c05009050110500e05015050150401105009050110500e05015050150401a0500a050130500e05016050
013e00202612527125291252b1252c1252c11518105181053012532125331253512537125371152210522105181251a1251b1251d1251f1251f1252c1252c115261252912527125291252c1252c1252712527115
003e00200c135181350c135181350c135181350c135181351413520135141352013514135201352213522135131351f135131351f135131351f1352013520135111351d135111351d1350e1351a1350f1351b115
00520000157641a7641d764217641d7641a7641576426764217641d7641a76415761167641a7641f764227641f7641a7641676428764217641f7641a76416761157641a7641d7642176426764217641d7641a764
005200001576421764157641d764167641a7641f7642276426764217641f7641a764167641f764167641a764157641a7641d764217641d7641a7641576426764217641d7641a76415761167641a7641f76422764
005200001f7641a7641676428764217641f7641a76416761157641a7641d7642176426764217641d7641a7641576421764157641d764167641a7641f764227642676421764217642176421764217642176421764
002900002176416764167641a7641a7641f7641f76422764227642676426764267642676416764167641a7641a7641f7641f76421764217642876428764287642876428764287642876428764287642876128735
01520020130500e0500a050150500a050130500e0501c05009050110500e05015050150401105009050110500e04015040150401a0400a040130400e040160401304015040150401503015030150201502015015
012000201573515736157351573515735157351573415700157351573615735157361573515736157351574215735157301573515735157351573615735157001573516735157351673515735167351573215752
013e002013122131220f1220f1221312213122181221812214122141220e1221a12214122141221b1221b1220c1220c1220f1221b1220f1220e12218122181220e1220e1220f1220f12211122111220f1220f122
004000201263518102016050060312605006030060535615126350160301605006031260500603006053561512635016030160512603126350060500605006351263501603016050060312635356050060535615
0180002028744287142f744307442d7442d7342d7242d7142f7442f7342f7242f7142b7442b7242a7442a72428744287142f744307442d7442d7342d7242d7142b7442b7242d7442d7242f734307342f71430712
0180002028742287122f742307422d7422d7322d7222d7122f7422f7222b7122b74228742287122f742307422d7422d7322d7222d7122f7222f7322f7222f7122b7222b7422d7222d7422f722307222f71230712
0014000027511255112751125515255112f5012d501315013c501375012f5012d5012a5012d501335013350136501375010050100501005010050100501005010050100501005010050100501005010050100501
001400001552115521175211652515521005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501
00140000095310b531095310a53509531005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501005010050100501
000f00001f511235112151124515245112051120511185111e5011c5111c511205111e5111a511205111c51120511355013250130501215011d5011a50117501125010f5010d5010a50108501045010250100501
000f00001852115521155211752513521155211752115521185211a5211a5211850118521195211c521145211552113521155211a501195011850116501135011150111501005010050100501005010050100501
000f0000105310e5310c5310e5350a5310c531105310f5310f5010e5310c5310d5310e5310c5310e5310e5310d5310b5310a5310d501005010050100501005010050100501005010050100501005010050100501
000f00001f51123511215111b5151d5111a511205011b511175111c5111c511175111b5111b51114511145111a51119511195011851118511165111c5111b511125010f5010d5010a50108501045010250100501
000f0000115211152111521115250f521115211750112521125211252112521135211452115521155211352112521135010f52110521115211152112521115211152111501005010050100501005010050100501
000f0000085310853108531075350b5310d5310b5310e53109531095310c5010b5310b5310b531095310b5310d5310f5310e5310a531005010c5310c5310c5310b53100501005010050100501005010050100501
000f00001c5111851125511235151e5111a511205111b511225111f5111d511215111e5111b5111b511205111c51120511195011751121511155111f5111e511185110f5010d5010a50108501045010250100501
000f0000145211d5211652113525185211a5211552115521185211a5211352119521195211d521155211c521155211350118521155211a5211b5211b521185211852111501005010050100501005010050100501
000f0000085310853108531075350b5310d5310b5310e53109531095310c5310b5310b5310b531095310b5310d5310f5310e5310a531005010c5310c5310c5310b53100501005010050100501005010050100501
01200000213151f3152231522315213151f3151f3141f3142631524315273152731526315243152431424314213151f3152231522315213151f3151f3141f3142631527315243152731526315243152431424314
01200000214121d4121d4121c412214121c412214121a4121c4121d4121d4121c4121a4121a4121c4121d412214121d4121d4121c412214121f4121f4121f4121a4121d4121d4121c4121a4121d4121a4121c412
003e0020261142711427114261142c1242c114181041810430114301143311433114371143711422104221042611426114271142711430114301141d1041d1043211432114331143311424124241143012430114
013e00200c134181340c134181340c134181340c134181341413420134141342013414134201342213422134131341f134131341f134131341f1342013420134111341d134111341d1340e1341a1340f1341b135
003e00202611427114291142b1142c1242c11418104181043011432114331143511437124371142210422104181141a1141b1141d1141f1141f1142c1242c114261142911427114291142c1142c1142712427114
015000201a6121a6121a6120e6120e6120e6120e6120e6120e6120e6120e6120e6120e6120e6121a6121a6121a6121a6121a6121a6121a6121a6121b612196121a6121a6120f6120f6120f6120f6120f6120f612
0150002002612016120161202612036120b6120961208612066120f612176121561214612126121261212612126121461215612176120d6120f6120f61212612116120e6120e6121761213612116120e61217612
015000201a61219612196121a6121b6122361221612206121e6121b6122361221612206121e6121e61212612126121461215612176120d6120f6120f61212612116120e6120e6121761213612116120e61217612
013e00200c135181050c135181050c135181050c135181051413520105141352010514135201352210522135131351f105131351f105131351f1352010520135111351d105111351d1050e1351a1050f1351b105
00520000097640e7641176415764117640e764097641a76415764117640e764097610a7640e7641376416764137640e7640a7641c76415764137640e7640a761097640e76411764157641a76415764117640e764
00520000097641576409764117640a7640e76413764167641a76415764137640e7640a764137640a7640e764097640e7641176415764117640e764097641a76415764117640e764097610a7640e7641376416764
00520000137640e7640a7641c76415764137640e7640a761097640e7641d764157641a76415764117640e764097641576409764117640a7640e76413764167641a76415764137640e7640a764137640a7640e764
0040102028754287342873428724287141c70410704107041070410704107041c704180051c005180051700510005170051c00517005180051c00518005170051e0052100512005150051c0051f0052800513005
014000201263518102016050060312605006031d615356151263501603016050060312605006031d615356151263501603016051260312635006051d615356151263501603016050060312635356051d61535615
0140002010035170351c03517035180351c03518035170351e0352103512035150351c0351f035280351303510035170351c03517035180351c03518035170351e0352103512035150351c0351f0352803513035
014000001532510325103251f325213251c3252332517325233251e3251e3251f325213251732518325183251532510325103251f325213251c3252332517325233251e3251e3251f32521325173251832518325
0140002010025170251c02517025180251c02518025170251e0252102512025150251c0251f025280251302510025170251c02517025180251c02518025170251e0252102512025150251c0251f0252802513025
014000001533210332103321f332213321c3322333217332233321e3321e3321f332213321733218332183321533210332103321f332213321c3322333217332233321e3321e3321f33221332173321833218332
0140002010032170321c03217032180321c03218032170321e0322103212032150321c0321f032280321303210032170321c03217032180321c03218032170321e0322103212032150321c0321f0322803213032
01200020126350c635126351e63512605006031d62535625126350c635126351e63512605006031d62535625126350c635126351e63512635006051d62535625126350c635126351e63512635356251d62535625
00800020102051720510205172050c205102050c20517205122051520512205152051c2051f205102051320510205172051020523205182051020518205172051e2052120512205152051c2051f2051020513205
014000201c2272322724227282272a2272d227282271f2272a2272d2271e22721227282272b227342271f2271c2272322724227282272a2272d227282271f2272a2272d2271e22721227282272b227342271f227
014000201c027230272802723027240272802724027230272a0272d0271e02721027280272b027340271f0271c027230272802723027240272802724027230272a0272d0271e02721027280272b027340271f027
01200020126450c635126051e645126051d6251d64535625126451e6351e6051e6451d605356451d60535645126450c635356450563512605006051d64535625126450c635126451e63512645356251d64535645
0120000015756157561575116752157561375613756137561a7561a7561a7511b7520e756187561875618756151461514615141161421514613146131461314621136211362113122132211361f1361f1361f136
01200020157500e7501075011750107500e7501575010750157501514015750151300e75011750107500e7501575011750107500e75010750117501113011150107601013010750101501d1501c7501a15011150
012000000955005550055500455009550045500955002550045500555005550045500255005550045500255009550055500555004550095500455009550025500455005550055500455002550055500455002550
01200020212221a2221c2221d2221c2221a222212221c222212272121721227212171a2221d2221c2221a222212221d2221c2221a2221c2271d2271d2171d2271c2271c2171c2271c2271d2161c2261a2161d216
01300000097640e7641176415764117640e764097641a76415764117640e764097610a7640e7641376416764137640e7640a7641c76415764137640e7640a761097640e76411764157641a76415764117640e764
01300000097641576409764117640a7640e76413764167641a76415764137640e7640a764137640a7640e764097640e7641176415764117640e764097641a76415764117640e764097610a7640e7641376416764
01300000137640e7640a7641c76415764137640e7640a761097640e7641d764157641a76415764117640e764097641576409764117640a7640e76413764167641a76415764137640e7640a764137640a7640e764
001300002176416764167641a7641a7641f7641f76422764227642676426764267642676416764167641a7641a7641f7641f76421764217642876428764287642876428764287642876428764287642876128735
003000001f7641a7641676428764217641f7641a76416761157641a7641d7642176426764217641d7641a7641576421764157641d764167641a7641f764227642676421764217642176421764217642176421764
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000200001b0500b0500706006060050700507003070110700307002070020700307001770077700e7700377003770027700177001770017700177005760057600576004760037600276001750000400003000010
00ff00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000100001b0300b0300704006040050400504003050110500305002050020500305001750077500e7500375003750027500175001750017500175005740057400574004740037300273001720000200000000000
__music__
00 2e7e4344
01 02424a44
00 02034a4b
00 02035d4a
00 020a624a
00 020a224a
00 1c0a224a
00 1c0a2244
00 1c1d2244
00 1e0a2244
00 0a1d5c44
00 4d1d4344
00 2e424344
00 23424344
00 24424344
00 25424344
00 04004849
00 05014944
00 06084344
00 07424344
00 0c464344
00 0b0d4344
00 0b0c4344
00 0b264344
00 3e424344
00 2e424344
00 0b2a6744
00 29282744
00 29282744
00 2b422744
00 2b2c2d44
00 2b2c2d44
00 2b303144
00 302f3144
00 2b2c2d44
00 292c2744
00 2b2c3144
00 29282744
00 096c0b44
00 33492d44
00 33492d44
00 32332d44
00 32332744
00 096c0b44
00 33744344
00 35344344
00 35342744
00 35342d44
00 35343144
00 35342d44
00 35342744
00 75344344
00 335b4344
00 3e424344
00 36424344
00 37424344
00 38424344
00 36424344
00 37424344
00 3a424344
00 39424344
00 79424344
00 3e424344
00 41424344

